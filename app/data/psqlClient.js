'use strict';

const pg = require('pg');
const conString = 'postgres://postgres:sm01314@db:5432/pvp9';
const Promise = require('bluebird');

Promise.promisifyAll(pg, {
  filter: (methodName) => methodName === 'connect',
  multiArgs: true,
});
Promise.promisifyAll(pg);

const pool = function() {
  let close;
  return pg.connectAsync(conString).spread(function(client, done) {
    close = done;
    return client;
  }).disposer(function() {
    if (close) {
      close();
    }
  });
};

function PromisedPGClient() {}

PromisedPGClient.prototype.queryFirstNode = function(sql, params) {
  return Promise.using(pool(), function(conn) {
    return conn.queryAsync(sql, params);
  }).then(function(result) {
    return result.rows[0];
  });
};

PromisedPGClient.prototype.queryCount = function(sql, params) {
  return this.queryFirstNode(sql, params).then(function(result) {
    return result.count || NaN;
  });
};

PromisedPGClient.prototype.query = function(sql, params) {
  return Promise.using(pool(), function(conn) {
    return conn.queryAsync(sql, params);
  }).then(function(result) {
    return result.rows;
  });
};

module.exports = PromisedPGClient;
