import PromisedPGClient from './psqlClient';
import geoip from 'geoip-lite';
import esc from 'pg-escape';
import  * as Util from './util';
const pgClient = new PromisedPGClient();

const ProductOrderByEnum = {
  SALES_HIGH_TO_LOW: 0b00010,
  SALES_LOW_TO_HIGH: 0b00100,
  PRICE_LOW_TO_HIGH: 0b01000,
  PRICE_HIGH_TO_LOW: 0b10000,
};

function buildOrderBy(orderBy) {
  switch (orderBy) {
    case 'SALES_HIGH_TO_LOW':
      return ' ORDER BY average_pageview_daily DESC, average_sales_daily DESC, sale_price ASC';
    case 'SALES_LOW_TO_HIGH':
      return ' ORDER BY average_pageview_daily DESC, average_sales_daily ASC, sale_price ASC';
    case 'PRICE_LOW_TO_HIGH':
      return ' ORDER BY sale_price ASC, average_pageview_daily DESC, average_sales_daily DESC';
    case 'PRICE_HIGH_TO_LOW':
      return ' ORDER BY sale_price DESC, average_pageview_daily DESC, average_sales_daily DESC';
    default:
      return ' ORDER BY average_pageview_daily DESC, average_sales_daily DESC, sale_price ASC';
  }
}

function buildLimitOffset(limitOffset) {
  const { limit, offset } = limitOffset;
  return ` LIMIT ${limit} OFFSET ${offset}`;
}

const Rank = function() {};

Rank.prototype.addProductPageviewByUpc = function(upc) {
  const sql = `
  INSERT INTO vendor_product_record (vendor_product_id, record_date, pageview)
    SELECT id, CURRENT_DATE AS record_date, 1 FROM vendor_product WHERE upc = $1
    ON CONFLICT ON CONSTRAINT vendor_product_record_vendor_product_id_record_date_key DO
    UPDATE SET pageview = vendor_product_record.pageview + 1;`
  return pgClient.query(sql, [upc]);
};

Rank.prototype.getProductById = function(upc) {
  const sql = 'SELECT * FROM product WHERE upc = $1'
  return pgClient.queryFirstNode(sql, [upc]).then((product) => {
    if (product) {
      this.addProductPageviewByUpc(upc);
    }
    return product;
  });
};

Rank.prototype.getVendorById = function(vid) {
  const sql = 'SELECT * FROM vendor WHERE id = $1';
  return pgClient.queryFirstNode(sql, [vid]);
};

Rank.prototype.getVendorProductById = function(id) {
  const sql = 'SELECT * FROM vendor_product WHERE id = $1';
  return pgClient.queryFirstNode(sql, [id]);
};

Rank.prototype.getStoreById = function(sid) {
  const sql = 'SELECT * FROM vendor_store WHERE id = $1';
  return pgClient.queryFirstNode(sql, [sid]);
};

Rank.prototype.getCategoryMappingById = function(cmid) {
  const sql = 'SELECT * FROM vendor_category WHERE id=$1;';
  return pgClient.queryFirstNode(sql, [cmid]);
};

Rank.prototype.getCategoryById = function(cid) {
  const sql = 'SELECT * FROM category WHERE id= $1';
  return pgClient.queryFirstNode(sql, [cid]);
};

Rank.prototype.getBrandNamesByCategoryId = function(cid) {
  const sql = `
  SELECT
    pc.category_id, ARRAY_AGG(DISTINCT p.brand_name)
    FROM product_category AS pc
    JOIN product AS p ON pc.upc = p.upc
    WHERE pc.category_id IN (SELECT id FROM get_itself_and_children_categories_by_parent_id($1))
    GROUP BY pc.category_id
  `;
  return pgClient.queryFirstNode(sql, [cid]);
};

Rank.prototype.getProductsListByCateId = function(cid, orderBy, limitOffset) {
  const sql = `
  WITH get_product_upc_by_category_id AS (
    SELECT DISTINCT upc
    FROM product_category
    WHERE category_id IN (SELECT id FROM get_itself_and_children_categories_by_parent_id($1))
  )
  SELECT p.* FROM get_product_upc_by_category_id as pc
  JOIN product AS p ON p.upc = pc.upc` + buildOrderBy(orderBy) + buildLimitOffset(limitOffset);
  return pgClient.query(sql, [cid]);
};

Rank.prototype.getProductsListCountByCateId = function(cid) {
  const sql = `
  SELECT
    count(DISTINCT p.upc)
    FROM product AS p
    JOIN product_category AS pc ON pc.upc = p.upc
    WHERE pc.category_id IN (SELECT id FROM get_itself_and_children_categories_by_parent_id($1))`;
  return pgClient.queryCount(sql, [cid]);
};

Rank.prototype.getVendorByUpc = function(upc) {
  const sql =
   `SELECT  v.*, vp.id AS vpid, vp.sku, vp.is_product_on_sale,
   vp.regular_price, vp.sale_price, vp.customer_rating,
   vp.customer_rating_count, vp.source_url
   FROM vendor_product AS vp
   JOIN vendor AS v on v.id = vp.vendor_id
   WHERE vp.upc = ($1)
   ORDER BY vp.sale_price ASC;`
  return pgClient.query(sql, [upc]);
};

Rank.prototype.getVendorProductByUpc = function(upc) {
  const sql = `SELECT * FROM vendor_product WHERE upc = ($1) ORDER BY sale_price ASC;`
  return pgClient.query(sql, [upc]);
};

Rank.prototype.getShippingInfoByVpid = function(vpid) {
  const sql =
    `SELECT * FROM vendor_product_shipping
    WHERE vendor_product_id=($1) ORDER BY last_modified DESC LIMIT 1`;
  return pgClient.queryFirstNode(sql, [vpid]);
};

Rank.prototype.getHoursByStoreId = function(sid) {
  const sql =
    `SELECT vsh.* FROM vendor_store_hours as vsh
    JOIN vendor_store AS vs ON vsh.vendor_store_id=vs.id
    WHERE vs.id=($1)`
  return pgClient.query(sql, [sid]);
};

Rank.prototype.getPickupInfoByVpid = function(sid, vpid) {
  const sql = `SELECT *
    FROM vendor_product_store_pickup
    WHERE vendor_store_id=$1 AND vendor_product_id=$2 ORDER BY last_modified DESC LIMIT 1`
  return pgClient.queryFirstNode(sql, [sid, vpid]);
};

Rank.prototype.getPickupsByVpidAndIp = function(vpid, ip, limitOffset) {
  const geo = geoip.lookup(ip);
  // get lat lng by geo lookup or default to vancouver center
  const latLng = geo && geo.ll && geo.ll.length === 2 ? geo.ll : [49.2891158, -123.1168909];
  const sql = `
  WITH get_pickup_info AS (
    SELECT
      id,
      vendor_product_id as vpid,
      vendor_store_id as sid,
      has_inventory,
      quantity,
      ROW_NUMBER() OVER(PARTITION BY vendor_product_id, vendor_store_id ORDER BY last_modified DESC) AS rn
      FROM vendor_product_store_pickup
      WHERE vendor_product_id=$1
  )
  SELECT
    vpsp.*,
    ROUND(CAST(ST_Distance_Sphere(vs.geom, ST_GeomFromText($2, 4326))/1000 As numeric), 2) As distance
    FROM get_pickup_info AS vpsp
    JOIN vendor_store AS vs ON vs.id=vpsp.sid
    WHERE vpsp.rn = 1
    ORDER BY distance` + buildLimitOffset(limitOffset);
  return pgClient.query(sql, [vpid, esc('POINT(%s %s)', latLng[1], latLng[0])]);
};

Rank.prototype.getPickupsCountByVpid = function(vpid) {
  const sql = `
  WITH get_pickup_info AS (
    SELECT
      id,
      vendor_product_id as vpid,
      vendor_store_id as sid,
      has_inventory,
      quantity,
      ROW_NUMBER() OVER(PARTITION BY vendor_product_id, vendor_store_id ORDER BY last_modified DESC) AS rn
      FROM vendor_product_store_pickup
      WHERE vendor_product_id=$1
  )
  SELECT count(*) FROM get_pickup_info AS vpsp WHERE vpsp.rn = 1;`;
  return pgClient.queryCount(sql, [vpid]);
};

Rank.prototype.getCategoryList = function() {
  const sql = 'SELECT * FROM category';
  return pgClient.query(sql, []);
};

Rank.prototype.getCategoriesByParentId = function(cid) {
  const sql = 'SELECT * FROM category WHERE parent_id = $1';
  return pgClient.query(sql, [cid]);
};

Rank.prototype.getCategoriesByUpc = function(upc) {
  const sql = 'SELECT * FROM get_category_path_by_upc($1) ORDER BY id';
  return pgClient.query(sql, [upc]);
};

Rank.prototype.getCategoriesById = function(categoryId) {
  const sql = 'SELECT * FROM get_all_categories_by_category_id($1) ORDER BY id';
  return pgClient.query(sql, [categoryId]);
};

Rank.prototype.getCategoryMappingList = function() {
  const sql = 'SELECT * FROM vendor_category';
  const params = [];
  return pgClient.query(sql, params);
};

Rank.prototype.getPickupById = function(id) {
  const sql = `
  SELECT id,
    vendor_product_id AS vpid,
    vendor_store_id AS sid,
    has_inventory,
    quantity
   FROM vendor_product_store_pickup WHERE id=$1;`;
  return pgClient.queryFirstNode(sql, [id]);
};

Rank.prototype.insertCategory = function(cateObj) {
  const sql = 'INSERT INTO category VALUES (DEFAULT, ($1), ($2), ($3), ($4)) RETURNING id;';
  return pgClient.queryFirstNode(sql, [ cateObj.cname, cateObj.ename, cateObj.isActive, cateObj.parentId ]);
};

Rank.prototype.insertCategoryMapping = function(cmObj) {
  const sql = 'INSERT INTO vendor_category VALUES (($1), ($2), ($3)) RETURNING id';
  return pgClient.queryFirstNode(sql, [ cmObj.vendorId, cmObj.categoryId, cmObj.vendorCategoryId ]);
};

Rank.prototype.getProductsByCondition = function(query, orderBy, limitOffset) {
  query = Util.queryEscape(query);
  const sql = `SELECT * FROM product WHERE document @@ to_tsquery('simple', $1)` + buildOrderBy(orderBy) + buildLimitOffset(limitOffset);
    // ORDER BY ts_rank(document, to_tsquery('simple', $1)) DESC
  return pgClient.query(sql, [query.split(' ').join(' & ')]);
};

Rank.prototype.getProductsCountByCondition = function(query) {
  query = Util.queryEscape(query);
  const sql = `SELECT count(*) FROM product WHERE document @@ to_tsquery('simple', $1)`;
  return pgClient.queryCount(sql, [query.split(' ').join(' & ')]);
};

Rank.prototype.fuzzyMatchWord = function(searchText, limit) {
  if (!searchText || searchText === '') {
    return [];
  }

  searchText = Util.queryEscape(searchText);

  const words = searchText.split(' ');
  const word = words[words.length - 1];
  const remain = searchText.substring(0, searchText.length - word.length);
  const sql = `
    SELECT * FROM word
    WHERE similarity(word, $1) > 0.3
    ORDER BY word <-> $1 LIMIT $2;`
  return pgClient.query(sql, [word, limit]).then((words) => {
    return words.map(({word}) => ({word: remain + word}));
  });
};

module.exports = Rank;
