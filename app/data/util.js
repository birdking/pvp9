export function indexToWeekday(idx) {
  const weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  return weekday[idx];
}

export function AMPMToHours(AMPM) {
  let ampm = AMPM.split(' ');
  let hh = parseInt(ampm[0]);
  let type = ampm[1];
  if (type === 'AM') {
    if (hh === 12) {
      hh = 0;
    }
  } else {
    if (hh !== 12) {
      hh+=12;
    }
  }

  return hh;
}

export function queryEscape(query) {
  if (!query || query === '') {
    return '';
  }

  return query.replace(/[　,]/g, ' ').trim().replace(/\s\s+/g, ' ').replace(/[^-_\s\w\u4e00-\u9fa5]/g,'');
}
