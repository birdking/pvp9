import {
  GraphQLBoolean,
  GraphQLFloat,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
} from 'graphql';

import {
  connectionArgs,
  connectionDefinitions,
  connectionFromArray,
  connectionFromPromisedArray,
  connectionFromArraySlice,
  connectionFromPromisedArraySlice,
  cursorToOffset,
  cursorForObjectInConnection,
  fromGlobalId,
  globalIdField,
  mutationWithClientMutationId,
  nodeDefinitions,
  offsetToCursor,
  toGlobalId,
} from 'graphql-relay';

import Rank from './rank';
import Promise from 'bluebird';
import  * as Util from './util';
const today = Util.indexToWeekday(new Date().getDay());

function makeQueryFromConnectionArgs(args) {
  const {first, after} = args;

  let query = {
    offset: 0,
    limit: 10,
  };

  if (first) {
    query.limit = first;
  }

  if (after) {
    query.offset = cursorToOffset(after) + 1;
  }

  return query;
}

function makeMetaFromOffsetCount(offset, count) {
  return {
    sliceStart: offset,
    arrayLength: count,
  };
}

const {nodeInterface, nodeField} = nodeDefinitions(
  (globalId) => {
    const target = fromGlobalId(globalId);
    if (target.type === 'Viewer') {
      return {id: 'guest'};
    } else if (target.type === 'Pickup') {
      return (new Rank()).getPickupById(target.id);
    } else if (target.type === 'Product') {
      return (new Rank()).getProductById(target.id);
    } else if (target.type === 'VendorProduct') {
      return (new Rank()).getVendorProductById(target.id);
    } else if (target.type === 'Category') {
      return (new Rank()).getCategoryById(target.id);
    } else if (target.type === 'Vendor') {
      return (new Rank()).getVendorById(target.id);
    } else if (target.type === 'Shipping') {
      return (new Rank()).getShippingInfoByvpid(target.id);
    } else if (target.type === 'Store') {
      return (new Rank()).getStoreById(target.id);
    } else if (target.type === 'CategoryMapping') {
      return (new Rank()).getCategoryMappingById(target.id);
    }
    return null;
  },
  (obj) => {
    return obj.sku ? vendorProductType :
      obj.upc ? productType :
      obj.icon_url ? vendorType :
      obj.parent_id ? categoryType :
      obj.purchasable ? shippingType :
      obj.address ? storeType :
      obj.has_inventory ? pickupType :
      obj.day ? hoursType :
      obj.vendor_category_id ? categoryMappingType : ViewerType;
  }
);

const hoursType = new GraphQLObjectType({
  name: 'Hours',
  fields: () => ({
    id: globalIdField('Hours'),
    day: {
      type: GraphQLString,
      description: 'Which day this hours represent to',
    },
    open: {
      type: GraphQLString,
      description: 'open from',
    },
    close: {
      type: GraphQLString,
      description: 'close at',
    },
    isToday: {
      type: GraphQLBoolean,
      description: 'is it today?',
      resolve: (hour) => {
        return hour.day === today ? true : false;
      },
    },
    isOpen: {
      type: GraphQLBoolean,
      description: 'is the store current open?',
      resolve: (hour) => {
        const open = Util.AMPMToHours(hour.open);
        const close = Util.AMPMToHours(hour.close);
        const now = new Date().getHours();
        return hour.day === today && now >= open && now < close ? true : false;
      },
    },
  }),
  interfaces: [nodeInterface],
});

const storeType = new GraphQLObjectType({
  name: 'Store',
  fields: () => ({
    id: globalIdField('Store'),
    sid: {
      type: GraphQLString,
      description: 'id of the store',
      resolve: (_) => _.id,
    },
    name: {
      type: GraphQLString,
      description: 'Name of the store',
    },
    address: {
      type: GraphQLString,
      description: 'Address of the store',
    },
    city: {
      type: GraphQLString,
      description: 'The city that store belongs to',
    },
    state: {
      type: GraphQLString,
      description: 'Which state that store belongs to',
    },
    country: {
      type: GraphQLString,
      description: 'Which country that store belongs to',
    },
    postalCode: {
      type: GraphQLString,
      description: 'OMG is this necessary',
      resolve: (store) => store.postal_code,
    },
    phone: {
      type: GraphQLString,
      description: 'The store\'s phone number',
    },
    hoursList: {
      type: new GraphQLList(hoursType),
      description: 'List of all the hours the store open in a week',
      resolve: (store) => {
        return (new Rank()).getHoursByStoreId(store.id)
        .then(harr => {
          return harr.map(ele => {
            ele.id = ele.sid + ele.day
            return ele;
          })
        });
      },
    },
  }),
  interfaces: [nodeInterface],
});

const pickupType = new GraphQLObjectType({
  name: 'Pickup',
  fields: () => ({
    id: globalIdField('Pickup'),
    vpid: {
      type: GraphQLInt,
      description: 'vendor product id',
    },
    sid: {
      type: GraphQLInt,
      description: 'vendor store id',
    },
    hasInventory: {
      type: GraphQLBoolean,
      description: 'Is there any goods in the inventory',
      resolve: (_) => _.has_inventory,
    },
    quantity: {
      type: GraphQLInt,
      description: 'How many Product of this kind is in Stock for pickup',
    },
    distance: {
      type: GraphQLFloat,
      description: 'The distance from store to user',
    },
    store: {
      type: storeType,
      description: 'pickup store',
      resolve: (_, args) => (new Rank()).getStoreById(_.sid),
    },
  }),
  interfaces: [nodeInterface],
});

const { connectionType: pickupConnectionType } = connectionDefinitions({ name: 'Pickup', nodeType: pickupType });

const shippingType = new GraphQLObjectType({
  name: 'Shipping',
  fields: () => ({
    id: globalIdField('Shipping'),
    purchasable: {
      type: GraphQLBoolean,
      description: 'Is the item purchasable through shipping',
    },
    quantity: {
      type: GraphQLInt,
      description: 'How many Product of this kind is in Stock for shipping',
    },
  }),
  interfaces: [nodeInterface],
});


const vendorType = new GraphQLObjectType({
  name: 'Vendor',
  fields: () => ({
    id: globalIdField('Vendor'),
    name: {
      type: GraphQLString,
      description: 'name of the vendor',
    },
    iconUrl: {
      type: GraphQLString,
      description: 'Vendor\'s logo',
      resolve: (_) => _.icon_url,
    },
  }),
  interfaces: [nodeInterface],
});

const vendorProductType = new GraphQLObjectType({
  name: 'VendorProduct',
  fields: () => ({
    id: globalIdField('VendorProduct'),
    vpid: {
      type: GraphQLInt,
      description: 'id of that vendor product',
      resolve: (_) => _.id,
    },
    vendor: {
      type: vendorType,
      description: 'Then vendor of product',
      resolve: (_) => (new Rank()).getVendorById(_.vendor_id),
    },
    regularPrice: {
      type: GraphQLString,
      description: 'regluar price the vendor offers',
      resolve: (_) => _.regular_price,
    },
    salePrice: {
      type: GraphQLString,
      description: 'sale price the vendor offers',
      resolve: (_) => _.sale_price,
    },
    isProductOnSale: {
      type: GraphQLBoolean,
      description: 'The flag for weather the product is on sale or not',
      resolve: (_) => _.is_product_on_sale,
    },
    customerRating: {
      type: GraphQLFloat,
      description: 'The Customer Rating on this vendor\'s product',
      resolve: (_) => _.customer_rating,
    },
    sourceUrl: {
      type: GraphQLString,
      description: 'url of this product in vendor',
      resolve: (_) => _.source_url,
    },
    sku: {
      type: GraphQLString,
      description: 'sku number of the product in this vendor',
    },
    shipping: {
      type: shippingType,
      description: 'The sipping infomation vendor provides',
      resolve: (_) => (new Rank()).getShippingInfoByVpid(_.id),
    },
    pickups: {
      type: pickupConnectionType,
      description: 'List of pickup stores the Vendor has',
      args: connectionArgs,
      resolve: (_, args, { rootValue: { request } }) => {
        const limitOffset = makeQueryFromConnectionArgs(args);

        return Promise.all([
          (new Rank()).getPickupsByVpidAndIp(_.id, request.ip, limitOffset),
          (new Rank()).getPickupsCountByVpid(_.id),
        ]).spread((rows, count) => connectionFromArraySlice(rows, args, makeMetaFromOffsetCount(limitOffset.offset, count)));
      },
    },

  }),
  interfaces: [nodeInterface],
});

const productType = new GraphQLObjectType({
  name: 'Product',
  fields: () => ({
    id: globalIdField('Product', (obj, info) => obj.upc),
    pid: {
      type: GraphQLString,
      description: 'upc number of that product',
      resolve: (_) => _.upc,
    },
    upc: {
      type: GraphQLString,
      description: 'upc number of that product',
    },
    name: {
      type: GraphQLString,
      description: 'name of the product',
    },
    brandName: {
      type: GraphQLString,
      description: 'brand name of the product',
      resolve: (_) => _.brand_name,
    },
    brandIconUrl: {
      type: GraphQLString,
      description: 'brand image url of the product',
      resolve: (_) => _.brand_icon_url,
    },
    modelNumber: {
      type: GraphQLString,
      description: 'module number of the product',
      resolve: (_) => _.model_number,
    },
    smallImageUrlPrefix: {
      type: GraphQLString,
      description: 'Vendor\'s small image url prefix',
      resolve: (vendor) => vendor.small_image_url_prefix,
    },
    mediumImageUrlPrefix: {
      type: GraphQLString,
      description: 'Vendor\'s medium image url prefix',
      resolve: (vendor) => vendor.medium_image_url_prefix,
    },
    largeImageUrlPrefix: {
      type: GraphQLString,
      description: 'Vendor\'s large image url prefix',
      resolve: (vendor) => vendor.large_image_url_prefix,
    },
    isProductOnSale: {
      type: GraphQLBoolean,
      description: 'The flag for weather the product is on sale or not',
      resolve: (vendor) => vendor.is_product_on_sale,
    },
    regularPrice: {
      type: GraphQLString,
      description: 'regluar price the vendor offers',
      resolve: (vendor) => vendor.regular_price,
    },
    salePrice: {
      type: GraphQLString,
      description: 'sale price the vendor offers',
      resolve: (vendor) => vendor.sale_price,
    },
    customerRatingCount: {
      type: GraphQLInt,
      description: 'Vendor\'s large image url prefix',
      resolve: (vendor) => vendor.customer_rating_count,
    },
    customerRating: {
      type: GraphQLInt,
      description: 'Vendor\'s large image url prefix',
      resolve: (vendor) => vendor.customer_rating,
    },
    description: {
      type: GraphQLString,
      description: 'The description of that product',
    },
    specs: {
      type: GraphQLString,
      description: 'The tech specs object in string format',
      resolve: (product) => JSON.stringify(product.specs),
    },
    averageSalesDaily: {
      type: GraphQLFloat,
      description: 'The current ranking in a day',
      resolve: (_) => _.average_sales_daily,
    },
    categoryPath: {
      type: new GraphQLList(categoryType),
      description: 'A recursive category list',
      resolve: (product, args) => {
        return (new Rank()).getCategoriesByUpc(product.upc);
      },
    },
    vendorProducts: {
      type: new GraphQLList(vendorProductType),
      description: 'Infomation of vendors who sell the product',
      resolve: (product, args) => (new Rank()).getVendorProductByUpc(product.upc),
    },
    images: {
      type: new GraphQLList(GraphQLString),
      description: 'A list of product images',
    },
  }),
  interfaces: [nodeInterface],
});

const { connectionType: productsConnectionType } = connectionDefinitions({ name: 'Product', nodeType: productType });

const categoryType = new GraphQLObjectType({
  name: 'Category',
  description: 'A Category in Electronics',
  fields: () => ({
    id: globalIdField('Category'),
    cid: {
      type: GraphQLInt,
      description: 'id of the category',
      resolve: (category) => category.id,
    },
    cname: {
      type: GraphQLString,
      description: 'The Chinese name of the category.',
    },
    ename: {
      type: GraphQLString,
      description: 'The English name of the category.',
    },
    isActive: {
      type: GraphQLBoolean,
      description: 'Is this category active or not.',
      resolve: (_) => _.is_active,
    },
    parentId: {
      type: GraphQLInt,
      description: 'The parent id of this category',
      resolve: (_) => _.parent_id,
    },
    products: {
      type: productsConnectionType,
      description: 'The Products in that category',
      args: Object.assign({
        orderBy: {
          type: GraphQLString,
          defaultValue: 'SALES_HIGH_TO_LOW',
        },
      }, connectionArgs),
      resolve: (category, args) => {
        const limitOffset = makeQueryFromConnectionArgs(args);

        return Promise.all([
          (new Rank()).getProductsListByCateId(category.id, args.orderBy, limitOffset),
          (new Rank()).getProductsListCountByCateId(category.id),
        ]).spread((rows, count) => connectionFromArraySlice(rows, args, makeMetaFromOffsetCount(limitOffset.offset, count)));
      },
    },
    categoryPath: {
      type: new GraphQLList(categoryType),
      description: 'A recursive category list',
      resolve: (category, args) => (new Rank()).getCategoriesById(category.id),
    },
  }),
  interfaces: [nodeInterface],
});


const categoryMappingType = new GraphQLObjectType({
  name: 'CategoryMapping',
  description: 'A CategoryMapping in Electronics',
  fields: () => ({
    id: globalIdField('CategoryMapping'),
    cmid: {
      type: GraphQLString,
      description: 'id of the category mapping',
      resolve: (_) => _.id,
    },
    vendorId: {
      type: GraphQLInt,
      description: 'vendor id of the category mapping maps to',
      resolve: (_) => {
        return _.vendor_id;
      },
    },
    categoryId: {
      type: GraphQLInt,
      description: 'category id of the category mapping maps to',
      resolve: (_) => _.category_id,
    },
    vendorCategoryId: {
      type: GraphQLString,
      description: 'The vendor category map to',
      resolve: (_) => _.vendor_category_id,
    },
  }),
  interfaces: [nodeInterface],
});

const {
  connectionType: categoryConnectionType,
  edgeType: CategoryEdge,
} = connectionDefinitions({ name: 'Category', nodeType: categoryType });

const {
  connectionType: categoryMappingConnectionType,
  edgeType: CategoryMappingEdge,
} = connectionDefinitions({ name: 'CategoryMapping', nodeType: categoryMappingType });

const autoCompleteType = new GraphQLObjectType({
  name: 'AutoComplete',
  fields: () => ({
    word: {
      type: GraphQLString,
      description: 'A word match searchText',
    },
  }),
});

const ViewerType = new GraphQLObjectType({
  name: 'Viewer',
  description: 'The viwer',
  fields: () => ({
    id: globalIdField('Viewer'),
    autoComplete: {
      type: new GraphQLList(autoCompleteType),
      description: 'An array of words match searchText',
      args: {
        limit: {
          type: GraphQLInt,
          defaultValue: 10,
        },
        searchText: {
          type: GraphQLString,
          defaultValue: null,
        },
      },
      resolve: (_, args) => (new Rank()).fuzzyMatchWord(args.searchText, args.limit),
    },
    categories: {
      type: categoryConnectionType,
      description: 'A collection of categories',
      args: Object.assign({
        cid: {
          type: GraphQLInt,
          defaultValue: 1,
        },
      }, connectionArgs),
      resolve: (root, args) => connectionFromPromisedArray(
        (new Rank()).getCategoriesByParentId(args.cid),
        args
      ),
    },
    category: {
      type: categoryType,
      description: 'A category detail by id',
      args: {
        cid: {
          type: GraphQLInt,
          defaultValue: 1,
        },
      },
      resolve: (root, args) => (new Rank()).getCategoryById(args.cid),
    },
    categoryMappings: {
      type: categoryMappingConnectionType,
      description: 'A collection of category mappings',
      args: connectionArgs,
      resolve: (root, args) => connectionFromPromisedArray(
        (new Rank()).getCategoryMappingList(),
        args
      ),
    },
    products: {
      type: productsConnectionType,
      description: 'A product List by Condition',
      args: Object.assign({
        query: {
          type: GraphQLString,
        },
        orderBy: {
          type: GraphQLString,
          defaultValue: 'SALES_HIGH_TO_LOW',
        },
      }, connectionArgs),
      resolve: (root, args) => {
        const limitOffset = makeQueryFromConnectionArgs(args);

        return Promise.all([
          (new Rank()).getProductsByCondition(args.query, args.orderBy, limitOffset),
          (new Rank()).getProductsCountByCondition(args.query),
        ]).spread((rows, count) => connectionFromArraySlice(rows, args, makeMetaFromOffsetCount(limitOffset.offset, count)));
      },
    },
    product: {
      type: productType,
      description: 'A product detail by id',
      args: {
        pid: {
          type: new GraphQLNonNull(GraphQLString),
        },
      },
      resolve: (root, args) => (new Rank()).getProductById(args.pid),
    },
  }),
  interfaces: [nodeInterface],
});

const queryType = new GraphQLObjectType({
  name: 'Query',
  fields: () => ({
    viewer: {
      type: ViewerType,
      description: 'root',
      resolve: (_) => _,
    },
    node: nodeField,
  }),
});


const categoryMappingMutation = mutationWithClientMutationId({
  name: 'IntroduceCategoryMapping',
  inputFields: {
    vendorId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    categoryId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    vendorCategoryId: {
      type: new GraphQLNonNull(GraphQLString),
    },
  },
  outputFields: {
    newCategoryMappingEdge: {
      type: CategoryMappingEdge,
      resolve: (payload) => {
        return Promise.all([
          (new Rank()).getCategoryMappingList(),
          (new Rank()).getCategoryMappingById(payload.insertId),
        ])
        .spread((allCategoryMappings, newCategoryMapping) => {
          const newCategoryMappingStr = JSON.stringify(newCategoryMapping);
          const offset = allCategoryMappings.reduce((pre, ele, idx) => {
            if (JSON.stringify(ele) === newCategoryMappingStr) {
              return idx;
            }
            return pre;
          }, -1);

          return {
            cursor: offset !== -1 ? offsetToCursor(offset) : null,
            node: newCategoryMapping,
          };
        });
      },
    },
    Viewer: {
      type: ViewerType,
      resolve: () => (new Rank()).getCategoryList(),
    },
  },
  mutateAndGetPayload: (input) => {
    return (new Rank()).insertCategoryMapping(input)
    .then((res) => {
      return {
        insertId: res.id,
      };
    });
  },
});

const categoryMutation = mutationWithClientMutationId({
  name: 'IntroduceCategory',
  inputFields: {
    cname: {
      type: new GraphQLNonNull(GraphQLString),
    },
    ename: {
      type: new GraphQLNonNull(GraphQLString),
    },
    isActive: {
      type: new GraphQLNonNull(GraphQLBoolean),
    },
    parentId: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  outputFields: {
    newCategoryEdge: {
      type: CategoryEdge,
      resolve: (payload) => {
        return Promise.all([(new Rank()).getCategoryList(), (new Rank()).getCategoryById(payload.insertId)])
        .spread((allCategories, newCategory) => {
          // delete resturant.category;
          // delete resturant.rates;
          const newCategoryStr = JSON.stringify(newCategory);
          const offset = allCategories.reduce((pre, ele, idx) => {
            if (JSON.stringify(ele) === newCategoryStr) {
              return idx;
            }
            return pre;
          }, -1);

          return {
            cursor: offset !== -1 ? offsetToCursor(offset) : null,
            node: newCategory,
          };
        });
      },
    },
    Viewer: {
      type: ViewerType,
      resolve: () => (new Rank()).getCategoryList(),
    },
  },
  mutateAndGetPayload: (input) => {
    return (new Rank()).insertCategory(input)
    .then((res) => {
      return {
        insertId: res.id,
      };
    });
  },
});


const mutationType = new GraphQLObjectType({
  name: 'Mutation',
  fields: () => ({
    introduceCategory: categoryMutation,
    introduceCategoryMapping: categoryMappingMutation,
  }),
});

module.exports.schema = new GraphQLSchema({
  query: queryType,
  mutation: mutationType,
});
