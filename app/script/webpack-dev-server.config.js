const webpack = require('webpack');
const path = require('path');
const buildPath = path.resolve(__dirname, '../public');
const nodeModulesPath = path.resolve(__dirname, '../node_modules');
const TransferWebpackPlugin = require('transfer-webpack-plugin');

const config = {
  //Entry points to the project
  entry: [
    'webpack/hot/dev-server',
    'webpack/hot/only-dev-server',
    path.join(__dirname, '../frontend/app.jsx'),
  ],
  //Config options on how to interpret requires imports
  resolve: {
    extensions: ['', '.js', '.jsx'],
    //node_modules: ["web_modules", "node_modules"]  (Default Settings)
  },
  watchOptions: {
    poll: true
  },
  //Server Configuration options
  devServer:{
    contentBase: 'public/',  //Relative directory for base of server
    proxy: { '/graphql': 'http://app:3000' },
    devtool: 'eval',
    hot: true,        //Live-reload
    inline: true,
    host: '0.0.0.0',   // for docker container
    port: 3001,        //Port Number
  },
  devtool: 'eval',
  output: {
    path: buildPath,    //Path of output file
    filename: 'app.js',
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': '"development"',
      },
    }),
    //Enables Hot Modules Replacement
    new webpack.HotModuleReplacementPlugin(),
    //Allows error warnings but does not stop compiling. Will remove when eslint is added
    new webpack.NoErrorsPlugin(),
    //Moves files
    // new TransferWebpackPlugin([
    //   {from: 'public'},
    // ], path.resolve(__dirname, '../frontend')),
  ],
  module: {
    // Loaders to interpret non-vanilla javascript code as well as most other extensions including images and text.
    preLoaders: [
      {
        //Eslint loader
        test: /\.(js|jsx)$/,
        loader: 'eslint-loader',
        include: [path.resolve(__dirname, '../frontend')],
        exclude: [nodeModulesPath],
      },
    ],
    loaders: [
      {
        //React-hot loader and
        test: /\.(js|jsx)$/,  //All .js and .jsx files
        loaders: ['react-hot', 'babel'], //react-hot is like browser sync and babel loads jsx and es6-7
        exclude: [nodeModulesPath],
      },
      {
        test: /\.md$/,
        loader: 'raw-loader',
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader',
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        loader: 'url-loader?limit=8192',
      },
    ],
  },
  //eslint config options. Part of the eslint-loader package
  eslint: {
    configFile: '.eslintrc',
  },
};

module.exports = config;
