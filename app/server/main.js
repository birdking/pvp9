global.navigator = { userAgent: 'all' };

import express from 'express';
import path from 'path';
import graphQLHTTP from 'express-graphql';
import { schema } from '../data/schema';
import renderOnServer from './renderOnServer';

const PORT = process.env.PORT || 3000;
const DEBUG_GRAPHQL = process.env.DEBUG_GRAPHQL === 'true';

const app = express();
app.set('x-powered-by', false);
app.set('trust proxy', true);

app.use('/graphql', graphQLHTTP(req => {
  return ({
    schema: schema,
    rootValue: {request: req},
    graphiql: DEBUG_GRAPHQL,
    pretty: DEBUG_GRAPHQL,
  });
}));

// app.use(express.static(path.join(__dirname, 'static')));
app.use(express.static(path.resolve(__dirname, '../public'), { maxAge: '365d', index: null }));

// need for material-ui server-side-rendering
app.get('/*', renderOnServer);

app.listen(PORT, () => {
  console.log('node ' + process.version + ' listen on port ' + PORT + '(' + process.env.NODE_ENV + ')');
});
