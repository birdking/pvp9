require('babel-register');
require('babel-polyfill');

const Bluebird = require('bluebird');
global.Promise = Bluebird;

require('./main');
