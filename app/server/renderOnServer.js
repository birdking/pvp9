import npmPackage from '../package.json';
import IsomorphicRouter from 'isomorphic-relay-router';
import path from 'path';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import Relay from 'react-relay';
import {match} from 'react-router';
import getRoutes from '../frontend/AppRoutes';
import Helmet from 'react-helmet';
import Html from './Html';

const GRAPHQL_URL = `http://localhost:3000/graphql`;

Relay.injectNetworkLayer(new Relay.DefaultNetworkLayer(GRAPHQL_URL));

const renderPage = (data, props, currentLocale) => {
  const appHtml = ReactDOMServer.renderToString(
    <IsomorphicRouter.RouterContext {...props} />
  );
  const helmet = Helmet.rewind();
  const preloadedData = JSON.stringify(data);
  const preload = `<script>window.__LOCALE__='${currentLocale}'</script><script id="preload" type="application/json">${preloadedData}</script>`;
  const scriptHtml = `<script src="/app.js?v=${npmPackage.version}"></script>`;

  const docHtml = ReactDOMServer.renderToStaticMarkup(
    <Html
      appCssFilename={`/css/main.css?v=${npmPackage.version}`}
      bodyHtml={`<div id="app">${appHtml}</div>${preload}${scriptHtml}`}
      googleAnalyticsId={'UA-76811276-1'}
      helmet={helmet}
      isProduction={process.env.NODE_ENV === 'production'}
    />
  );
  return `<!DOCTYPE html>${docHtml}`;
};

export default (req, res, next) => {
  const currentLocale = req.acceptsLanguages(['zh', 'en']) || 'en'
  match({routes: getRoutes(currentLocale), location: req.originalUrl}, (error, redirectLocation, renderProps) => {
    if (error) {
      next(error);
    } else if (redirectLocation) {
      res.redirect(302, redirectLocation.pathname + redirectLocation.search);
    } else if (renderProps) {
      IsomorphicRouter.prepareData(renderProps).then(({data, props}) => {
        res.send(renderPage(data, props, currentLocale));
      }, next);
    } else {
      res.status(404).send('Not Found');
    }
  });
};
