import Colors from 'material-ui/lib/styles/colors';

export default {
  fontFamily: 'Roboto, sans-serif',
  palette: {
    primary1Color: Colors.purple700,
    primary2Color: Colors.purple800,
    primary3Color: '#f1f1f1',
    accent1Color: Colors.pinkA200,
    accent2Color: Colors.gray300,
    accent3Color: Colors.pinkA200,
    textColor: Colors.darkBlack,
    secondaryTextColor: Colors.lightBlack,
    disabledTextColor: Colors.minBlack,
    alternateTextColor: Colors.white,
    canvasColor: Colors.white,
    borderColor: Colors.grey300,
    disabledColor: Colors.minBlack,
  },
};
