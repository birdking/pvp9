import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory, useRouterHistory } from 'react-router';
import getRoutes from './AppRoutes';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { createHashHistory } from 'history';
import IsomorphicRelay from 'isomorphic-relay';
import IsomorphicRouter from 'isomorphic-relay-router';
// import en from 'react-intl/locale-data/en';
// import zh from 'react-intl/locale-data/zh';
// import { addLocaleData } from 'react-intl';

//Needed for React Developer Tools
window.React = React;

// github.com/yahoo/react-intl/wiki/Upgrade-Guide#add-call-to-addlocaledata-in-browser
// [en, zh].forEach(addLocaleData);

//Needed for onTouchTap
//Can go away when react 1.0 release
injectTapEventPlugin();

let history = useRouterHistory(createHashHistory)({queryKey: false});
if (process.env.NODE_ENV === 'production') {
  history = browserHistory;
  IsomorphicRelay.injectPreparedData(JSON.parse(document.getElementById('preload').textContent));
}

/**
 * Render the main app component. You can read more about the react-router here:
 */
ReactDOM.render(
  <IsomorphicRouter.Router
    history={history}>
    {getRoutes(window && window.__LOCALE__ || 'en')}
  </IsomorphicRouter.Router>,
  document.getElementById('app')
);
