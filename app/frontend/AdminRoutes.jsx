import React from 'react';
import Relay from 'react-relay';
import {Redirect, IndexRoute, IndexRedirect, Router, Route, Link} from 'react-router';
import Master, { prepareMasterParams } from './components/Master';
import Admin from './components/pages/Admin';
import CategoryList from './components/pages/CategoryList';
import CategoryMappingList from './components/pages/CategoryMappingList';

const CategoryListQueries = {
  viewer: () => Relay.QL`query { viewer }`,
};

export default (lang) => {
  return (
    <Route
      path="/"
      component={Master}
      queries={CategoryListQueries}
      stateParams={['lang']}
      defaultLang={lang}
      prepareParams={prepareMasterParams}>
      <Route path="admin" component={Admin} >
        <Route path="categoryList" component={CategoryList} queries={CategoryListQueries} />
        <Route path="categoryMappingList" component={CategoryMappingList} queries={CategoryListQueries} />
      </Route>
    </Route>
  );
};
