import Relay from 'react-relay';

export default class AddCategoryMutation extends Relay.Mutation {

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
      }`,
  };

  getMutation() {
    return Relay.QL`mutation{ introduceCategory }`;
  }

  getVariables() {
    return {
      cname: this.props.cname,
      ename: this.props.ename,
      isActive: this.props.isActive,
      parentId: this.props.parentId,
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on IntroduceCategoryPayload {
        Viewer {
          categories (first: 100) {
            edges {
              node {
                cid,
                cname,
                ename,
                isActive,
                parentId
              }
            }
          }
        },
        newCategoryEdge,
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'RANGE_ADD',
      parentName: 'Viewer',
      parentID: this.props.viewer.id,
      connectionName: 'categories',
      edgeName: 'newCategoryEdge',
      rangeBehaviors: {
        '': 'prepend',
      },
    }];
  }


}
