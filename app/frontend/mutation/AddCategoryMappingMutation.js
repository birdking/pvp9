import Relay from 'react-relay';

export default class AddCategoryMappingMutation extends Relay.Mutation {

  static fragments = {
    viewer: () => Relay.QL`
      fragment on Viewer {
        id,
      }`,
  };

  getMutation() {
    return Relay.QL`mutation{ introduceCategoryMapping }`;
  }

  getVariables() {
    return {
      vendorId: this.props.vendorId,
      categoryId: this.props.categoryId,
      vendorCategoryId: this.props.vendorCategoryId,
    };
  }

  getFatQuery() {
    return Relay.QL`
      fragment on IntroduceCategoryMappingPayload {
        Viewer {
          categoryMappings (first: 100) {
            edges {
              node {
                cmid,
                vendorId,
                categoryId,
                vendorCategoryId
              }
            }
          }
        },
        newCategoryMappingEdge,
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'RANGE_ADD',
      parentName: 'Viewer',
      parentID: this.props.viewer.id,
      connectionName: 'categoryMappings',
      edgeName: 'newCategoryMappingEdge',
      rangeBehaviors: {
        '': 'append',
      },
    }];
  }


}
