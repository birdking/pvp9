import React from 'react';
import SvgIcon from 'material-ui/lib/svg-icon';

export default class DoubleDown extends React.Component {

  render() {
    return (
      <SvgIcon {...this.props}>
        <path d="M16.59,5.59L18,7L12,13L6,7L7.41,5.59L12,10.17L16.59,5.59M16.59,11.59L18,13L12,19L6,13L7.41,11.59L12,16.17L16.59,11.59Z" />
      </SvgIcon>
    );
  }

}
