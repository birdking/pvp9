import React from 'react';
import SvgIcon from 'material-ui/lib/svg-icon';

export default class Equalizer extends React.Component {

  render() {
    return (
      <SvgIcon {...this.props}>
        <path d="M0 0h24v24H0z" fill="none"/>
        <path d="M10 20h4V4h-4v16zm-6 0h4v-8H4v8zM16 9v11h4V9h-4z"/>
      </SvgIcon>
    );
  }

}
