import React from 'react';
import Clearfix from 'material-ui/lib/clearfix';
import Mixins from 'material-ui/lib/mixins';

const { StyleResizable } = Mixins;

const ResponsiveGroup = React.createClass({

  displayName: 'ResponsiveGroup',

  propTypes: {
    children: React.PropTypes.node,
    float: React.PropTypes.oneOf(['left', 'right']),
    orientOnSmallScreen: React.PropTypes.oneOf(['horizontal', 'vertical']),
    defaultChildrenWidth: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number,
    ]),
    defaultChildrenHeight: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number,
    ]),
    autoOrient: React.PropTypes.bool,
  },

  contextTypes: {
    muiTheme: React.PropTypes.object,
  },

  mixins: [StyleResizable],

  getInitialState() {
    return {};
  },

  getDefaultProps() {
    return {
      float: 'left',
      orientOnSmallScreen: 'vertical',
      defaultChildrenWidth: '100%',
      defaultChildrenHeight: 'auto',
      autoOrient: false,
    };
  },

  render() {
    const {
      children,
      float,
      orientOnSmallScreen,
      defaultChildrenWidth,
      defaultChildrenHeight,
      autoOrient,
      ...other,
    } = this.props;

    const isLargeScreen = this.isDeviceSize(StyleResizable.statics.Sizes.MEDIUM);
    let defaultChildStyle = {float};
    if (autoOrient) {
      if ((isLargeScreen && orientOnSmallScreen === 'horizontal') ||
          (!isLargeScreen && orientOnSmallScreen === 'vertical')) {
          defaultChildStyle = Object.assign(defaultChildStyle, {width: defaultChildrenWidth});
      } else {
          defaultChildStyle = Object.assign(defaultChildStyle, {height: defaultChildrenHeight});
      }
    }

    const newChildren = React.Children.map(children, (child) => {
      if (React.isValidElement(child)) {
        const {
          style,
          children,
          ...other,
        } = child.props;
        const newProps = {
          style: Object.assign({}, style, defaultChildStyle),
          ...other,
        };
        return React.cloneElement(child, newProps, children);
      }
    }, this);

    return (
      <Clearfix {...other}>
        {newChildren}
      </Clearfix>
    );
  },
});

module.exports = ResponsiveGroup;
