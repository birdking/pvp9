import React from 'react';
import Transitions from 'material-ui/lib/styles/transitions';
import IconButton from 'material-ui/lib/icon-button';
import CodeIcon from 'material-ui/lib/svg-icons/action/code';
import Toolbar from 'material-ui/lib/toolbar/toolbar';
import ToolbarGroup from 'material-ui/lib/toolbar/toolbar-group';
import ToolbarTitle from 'material-ui/lib/toolbar/toolbar-title';

const styles = {
  markdown: {
    overflow: 'auto',
    // maxHeight: 1400,
    transition: Transitions.create('max-height', '800ms', '0ms', 'ease-in-out'),
    marginTop: 0,
    marginBottom: 0,
    width: '100%',
  },
  markdownRetracted: {
    maxHeight: 0,
  },
  codeBlockTitle: {
    borderTop: 'solid 1px #e0e0e0',
    background: 'white',
    cursor: 'pointer',
  },
};

const CodeBlock = React.createClass({
  propTypes: {
    children: React.PropTypes.node,
    title: React.PropTypes.string,
    insertHTML: React.PropTypes.bool,
    style: React.PropTypes.object,
    rootStyle: React.PropTypes.object,
  },
  getInitialState: function() {
    return {
      expand: false,
    };
  },
  handleTouchTap() {
    this.setState({
      expand: !this.state.expand,
    });
  },
  render() {
    const {
      children,
      title,
      insertHTML,
      style,
      rootStyle,
    } = this.props;
    let content = insertHTML ?
      <div dangerouslySetInnerHTML={{__html: children}} /> :
      children;


    let codeStyle = Object.assign({}, styles.markdown, styles.markdownRetracted);
    let tooltip = '展开';

    if (this.state.expand) {
      codeStyle = styles.markdown;
      tooltip = '收起';
    }

    codeStyle = Object.assign(codeStyle, style);
    return (
      <div style={rootStyle}>
        <Toolbar onTouchTap={this.handleTouchTap} style={styles.codeBlockTitle}>
          <ToolbarGroup float="left">
            <ToolbarTitle text={title || 'Specification'} />
          </ToolbarGroup>
          <ToolbarGroup float="right">
            <IconButton touch={true} tooltip={tooltip}>
              <CodeIcon />
            </IconButton>
          </ToolbarGroup>
        </Toolbar>
        <div style={codeStyle} >
          <div style={{padding: 15}}>
            {content}
          </div>
        </div>
      </div>
    );
  },
});

module.exports = CodeBlock;
