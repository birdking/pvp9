import React from 'react';
import { State } from 'react-router';
import List from 'material-ui/lib/lists/list';
import ListItem from 'material-ui/lib/lists/list-item';
import MenuItem from 'material-ui/lib/menus/menu-item';
import Menu from 'material-ui/lib/menus/menu';
import Mixins from 'material-ui/lib/mixins';
import Styles from 'material-ui/lib/styles';
import { SelectableContainerEnhance } from 'material-ui/lib/hoc/selectable-enhance';
const { Spacing, Colors } = Styles;
const { StyleResizable, StylePropable } = Mixins;


const SelectableList = SelectableContainerEnhance(List);

const PageWithNav = React.createClass({

  displayName: 'PageWithNav',

  propTypes: {
    children: React.PropTypes.node,
    location: React.PropTypes.object.isRequired,
    menuItems: React.PropTypes.array,
  },

  contextTypes: {
    muiTheme: React.PropTypes.object,
    router: React.PropTypes.object.isRequired,
  },


  mixins: [StyleResizable, StylePropable],

  getStyles(){
    let subNavWidth = Spacing.desktopKeylineIncrement * 3 + 'px';
    let styles = {
      root: {
        paddingTop: Spacing.desktopKeylineIncrement + 'px',
      },
      rootWhenMedium: {
        position: 'relative',
      },
      secondaryNav: {
        borderTop: 'solid 1px ' + Colors.grey300,
        overflow: 'hidden',
      },
      content: {
        boxSizing: 'border-box',
        padding: Spacing.desktopGutter + 'px',
      },
      secondaryNavWhenMedium: {
        borderTop: 'none',
        position: 'absolute',
        top: '64px',
        width: subNavWidth,
      },
      contentWhenMedium: {
        marginLeft: subNavWidth,
        borderLeft: 'solid 1px ' + Colors.grey300,
        minHeight: '800px',
      },
    };

    if (this.isDeviceSize(StyleResizable.statics.Sizes.MEDIUM) ||
        this.isDeviceSize(StyleResizable.statics.Sizes.LARGE)) {
      styles.root = this.mergeStyles(styles.root, styles.rootWhenMedium);
      styles.secondaryNav = this.mergeStyles(styles.secondaryNav, styles.secondaryNavWhenMedium);
      styles.content = this.mergeStyles(styles.content, styles.contentWhenMedium);
    }

    return styles;
  },


  onRequestChangeList(event, value) {
    this.context.router.push(value);
  },

  render() {
    const styles = this.getStyles();
    const { location } = this.props;
    return (
      <div style={this.prepareStyles(styles.root)}>
        <div style={this.prepareStyles(styles.secondaryNav)}>
          <SelectableList
            valueLink={{value: location.pathname, requestChange: this.onRequestChangeList}} >
            {this.props.menuItems.map((ele, idx) =>
              <ListItem
                key={idx}
                value={ele.route}
                primaryText={ele.text} />)}
          </SelectableList>
        </div>
        <div style={this.prepareStyles(styles.content)}>
          {this.props.children}
        </div>
      </div>
    );
  },

});

module.exports = PageWithNav;
