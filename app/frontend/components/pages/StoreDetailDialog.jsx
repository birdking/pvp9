import React from 'react';
import Dialog from 'material-ui/lib/dialog';
import FlatButton from 'material-ui/lib/flat-button';

const StoreDetailDialog = React.createClass({

  propTypes: {
    open: React.PropTypes.bool,
    vendor: React.PropTypes.object,
    onRequestClose: React.PropTypes.func,
    store: React.PropTypes.object,
    displayQuantity: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number,
    ]),
  },

  getHourList() {
    const {store} = this.props;
    return (
      store.hoursList.map((ele) => {
        let res;
        if (ele.isOpen && ele.isToday) {
          res = <p key={ele.id + 'hourList'} style={{color: 'green', fontWeight: 'bold'}}>Open now: {ele.open} - {ele.close}</p>
        } else if (ele.isToday) {
          res = <p key={ele.id + 'hourList'} style={{color: 'red'}}>Closed</p>
        } else {
          res = <p key={ele.id + 'hourList'} >{ele.day}: {ele.open} - {ele.close}</p>
        }
        return res;
      })
    );
  },

  render() {
    const actions = [
      <FlatButton
        label="OK"
        secondary={true}
        onTouchTap={this.props.onRequestClose}
      />,
    ];
    const { store, displayQuantity} = this.props;
    return (
      <Dialog
        ref="storeDetailDialog"
        title={'Store Details'}
        open={this.props.open}
        onRequestClose={this.props.onRequestClose}
        actions={actions}>
        <p>Name: {store.name}</p>
        <p>Address: {store.address}</p>
        <p>City: {store.city}</p>
        <p>Postal Code: {store.postalCode}</p>
        <p>Phone: {store.phone}</p>
        <p>Stock: {displayQuantity}</p>
        {this.props.store.hoursList && this.getHourList()}
      </Dialog>
    );
  },
});

module.exports = StoreDetailDialog;
