import React from 'react';
import Relay from 'react-relay';
import { Link } from 'react-router';
import Colors from 'material-ui/lib/styles/colors';
import EnhancedButton from 'material-ui/lib/enhanced-button';
import InfiniteScroll from '../InfiniteScroll';
import SelectField from 'material-ui/lib/select-field';
import MenuItem from 'material-ui/lib/menus/menu-item';
import ResponsiveGroup from '../ResponsiveGroup';
import ProductItem from './ProductItem';
import Helmet from 'react-helmet';
import stringTemplate from 'string-template';

const CategoryProduct = React.createClass({

  displayName: 'CategoryProduct',

  propTypes: {
    viewer: React.PropTypes.object,
    params: React.PropTypes.object,
    relay: React.PropTypes.any,
  },

  contextTypes: {
    muiTheme: React.PropTypes.object,
  },

  getInitialState() {
    return {
      isLoading: false,
    };
  },

  getStyles() {
    const {
      muiTheme: {
        rawTheme: {
          palette,
        },
      },
    } = this.context;

    return {
      root: {
        maxWidth: '1222',
        margin: '0 auto',
      },
      container: {
        border: '1px ' + Colors.faintBlack + ' solid',
        backgroundColor: '#fff',
        margin: 10,
      },
      padding: {
        padding: '15px',
      },
      inlineBlock: {
        display: 'inline-block',
      },
      headerTextDefault: {
        borderRight: '1px ' + Colors.faintBlack + ' solid',
      },
      headerTextActive: {
        color: palette.accent1Color,
      },
      breadcrumb: {
        borderBottom: '1px ' + Colors.faintBlack + ' solid',
      },
      productItem: {
        display: 'block',
        overflow: 'hidden',
        width: 200,
        padding: '15px 10px 10px 10px',
        borderTop: '1px ' + Colors.faintBlack + ' solid',
      },
      categoryItem: {
        display: 'block',
        overflow: 'hidden',
        width: 155,
        height: 175,
        textAlign: 'center',
      },
      categoryItemInner: {
        padding: '26px 20px',
      },
      categoryImage: {
        width: 100,
        height: 100,
      },
      categoryName: {
        fontSize: 14,
        lineHeight: '20px',
      },
    };
  },

  _handleOrderByChange(event, index, value) {
    if (!this.state.isLoading) {
      this.setState({isLoading: true}, () => {
        this.props.relay.setVariables({
          count: 12,
          orderBy: value,
        }, (readyState) => {
          if (readyState.done) {
            this.setState({isLoading: false});
          }
        });
      });
    }
  },

  _handleLoadMore() {
    if (!this.state.isLoading) {
      this.setState({isLoading: true}, () => {
        this.props.relay.setVariables({
          count: this.props.relay.variables.count + 10,
        }, (readyState) => {
          if (readyState.done) {
            this.setState({isLoading: false});
          }
        });
      });
    }
  },

  _getParentCategories(cid, categories, arr) {
    arr = arr || [];
    for (let i = 0; i < categories.length; i++) {
      if ('' + categories[i].cid === '' + cid) {
        arr.unshift(categories[i]);
        this._getParentCategories(categories[i].parentId, categories, arr);
      }
    }
    return arr;
  },

  _getSubCategories(cid, categories) {
    return categories.reduce((prev, next) => {
      if ('' + next.parentId === '' + cid) {
        prev.push(next);
      }
      return prev;
    }, []);
  },

  render() {
    const { category } = this.props.viewer;
    const { categoryPath, products } = category;
    const subCategories = this._getSubCategories(category.cid, categoryPath);
    const parentCategories = this._getParentCategories(category.cid, categoryPath);

    const styles = this.getStyles();

    return (
      <div style={styles.root}>
        <Helmet
          htmlAttributes={{lang: 'zh'}}
          title={category.ename || category.cname}
          titleTemplate="pvp9.com - %s"
          defaultTitle="Category"
          meta={[
            {name: 'description', content: category.ename || category.cname},
          ]} />
        <div style={styles.container}>
          <div style={styles.breadcrumb}>
            {parentCategories.map((parentCategory) => (
              <Link
                key={'_category' + parentCategory.cid}
                to={`/category/${parentCategory.cid}`}
                activeStyle={styles.headerTextActive}
                style={Object.assign({}, styles.padding, styles.inlineBlock, styles.headerTextDefault)}>
                {parentCategory.ename || parentCategory.cname}
              </Link>
            ))}
          </div>
          <ResponsiveGroup
            autoOrient={true}
            defaultChildrenWidth="50%">
            {subCategories.map((subCategory) => {
              const product = subCategory.products.edges[0].node;
              const productImage = product.images && product.images.length > 0 ? stringTemplate(product.mediumImageUrlPrefix, [product.images[0]]) : '//www.sewcurvy.com/siteimages/_templates/no-image.jpg';
              return (
                <EnhancedButton
                  linkButton={true}
                  key={'_category' + subCategory.cid}
                  containerElement={<Link to={`/category/${subCategory.cid}`}/>}
                  style={styles.categoryItem}>
                  <div style={styles.categoryItemInner}>
                    <img
                      src={productImage}
                      style={styles.categoryImage} />
                    <div style={styles.categoryName}>
                      {subCategory.ename || subCategory.cname}
                    </div>
                  </div>
                </EnhancedButton>
              );
            })}
          </ResponsiveGroup>
        </div>
        <div style={styles.container}>
          <div style={styles.padding}>
            <SelectField
              value={this.props.relay.variables.orderBy}
              onChange={this._handleOrderByChange}>
              <MenuItem value="SALES_HIGH_TO_LOW" primaryText="sales: high to low"/>
              <MenuItem value="SALES_LOW_TO_HIGH" primaryText="sales: low to high"/>
              <MenuItem value="PRICE_LOW_TO_HIGH" primaryText="price: low to high"/>
              <MenuItem value="PRICE_HIGH_TO_LOW" primaryText="price: high to low"/>
            </SelectField>
          </div>
          <InfiniteScroll
            hasMore={products.pageInfo.hasNextPage}
            loadMore={this._handleLoadMore}>
            <ResponsiveGroup
              autoOrient={true}
              defaultChildrenHeight={340}>
              {products.edges.map(({node: product}) => (
                <ProductItem
                  key={'_product' + product.upc}
                  containerElement={<Link to={`/product/${product.upc}`} />}
                  product={product}
                  style={styles.productItem} />
              ))}
            </ResponsiveGroup>
          </InfiniteScroll>
        </div>
      </div>
    );
  },

});

module.exports = Relay.createContainer(CategoryProduct, {
  initialVariables: {
    cid: 1,
    orderBy: 'SALES_HIGH_TO_LOW',
    count: 12,
  },

  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        category (cid: $cid) {
          cid,
          parentId,
          cname,
          ename,
          categoryPath {
            cid,
            parentId,
            cname,
            ename,
            products(first: 1) {
              edges {
                node {
                  name,
                  mediumImageUrlPrefix,
                  images
                }
              }
            }
          },
          products(first: $count, orderBy: $orderBy) {
            edges {
              node {
                upc,
                ${ProductItem.getFragment('product')}
              }
            },
            pageInfo {
              hasNextPage
            }
          }
        }
      }
    `,
  },
});

module.exports.prepareParams = (params, route) => Object.assign(params, {
  cid: params.cid ? parseInt(params.cid, 10) : 1,
});
