import React from 'react';
import Relay from 'react-relay';
import AddCategoryMappingMutation from '../../mutation/AddCategoryMappingMutation';
import {
  Dialog,
  Table,
  TableBody,
  TableFooter,
  TableHeader,
  TableRow,
  TableHeaderColumn,
  TableRowColumn,
  TextField,
  FlatButton,
  RaisedButton,
  SelectField,
  MenuItem,
} from 'material-ui';

const CategoryMappingList = React.createClass({

  displayName: 'VendorList',

  propTypes: {
    viewer: React.PropTypes.object,
  },

  contextTypes: {
    muiTheme: React.PropTypes.object,
    router: React.PropTypes.object.isRequired,
  },

  getInitialState() {
    return {
      addDialogOpenFlag: false,
    };
  },

  _onAddCategoryMapping(e) {
    e.preventDefault();
    this.setState({
      addDialogOpenFlag: true,
    });
  },

  _onDialogSubmit() {
    // if (this._checkAllEmpty()) {

    let vendorId = this.refs.vendorId.getValue();
    let categoryId = this.refs.categoryId.getValue();
    let vendorCategoryId = this.refs.vendorCategoryId.getValue();

    Relay.Store.commitUpdate(
      new AddCategoryMappingMutation({
        vendorId,
        categoryId,
        vendorCategoryId,
        viewer: this.props.viewer,
      })
    );
    this.setState({
      addDialogOpenFlag: false,
    });
    // }
  },

  _onDialogRequestClose() {
    this.setState({
      addDialogOpenFlag: false,
    });
  },

  getStyles(){
    let styles = {
      textField: {
        margin: '20px',
      },
    };
    return styles;
  },



  render() {

    const styles = this.getStyles();
    const standardActions = [
      <FlatButton
        label="取消"
        secondary={true}
        onTouchTap={this._onDialogRequestClose}
      />,
      <FlatButton
        label="添加"
        primary={true}
        onTouchTap={this._onDialogSubmit}
      />,
    ];

    const { categoryMappings } = this.props.viewer;
    return (
      <div>
        <Dialog
          ref="addDialog"
          title={'添加新的分类Mapping'}
          open={this.state.addDialogOpenFlag}
          onRequestClose={this._onDialogRequestClose}
          actions={standardActions}>
          <TextField
            ref="vendorId"
            style={styles.textField}
            hintText="对应商家ID"
            floatingLabelText="输入对应商家的ID"
          />
          <TextField
            ref="categoryId"
            style={styles.textField}
            hintText="对应我们的分类ID"
            floatingLabelText="输入对应我们的分类ID"
          />
          <TextField
            ref="vendorCategoryId"
            style={styles.textField}
            hintText="对应商家源分类ID"
            floatingLabelText="输入对应商家源分类ID"
          />
        </Dialog>
        <RaisedButton
          label="ADD CATEGORY MAPPING"
          primary={true}
          onTouchTap={this._onAddCategoryMapping}
        />
        <Table selectable={false}>
          <TableHeader
            adjustForCheckbox={false}
            displaySelectAll={false}>
            <TableRow>
              <TableHeaderColumn colSpan="4" style={{textAlign: 'center'}}>
                分类Mapping列表
              </TableHeaderColumn>
            </TableRow>
            <TableRow>
              <TableHeaderColumn>ID</TableHeaderColumn>
              <TableHeaderColumn>商家ID</TableHeaderColumn>
              <TableHeaderColumn>我们分类ID</TableHeaderColumn>
              <TableHeaderColumn>商家源分类ID</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody
            displayRowCheckbox={false}
            showRowHover={true}>
            {categoryMappings.edges.map((ele) => (
              <TableRow key={'_detail' + ele.node.cmid}>
                <TableRowColumn>{ele.node.cmid}</TableRowColumn>
                <TableRowColumn>{ele.node.vendorId}</TableRowColumn>
                <TableRowColumn>{ele.node.categoryId}</TableRowColumn>
                <TableRowColumn>{ele.node.vendorCategoryId}</TableRowColumn>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
    );
  },


});


module.exports = Relay.createContainer(CategoryMappingList, {
  initialVariables: {
    page: 0,
    limit: 12,
  },

  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        categoryMappings (first: 100) {
          edges {
            node {
              cmid,
              vendorId,
              categoryId,
              vendorCategoryId
            }
          }
        }
        ${AddCategoryMappingMutation.getFragment('viewer')},
      }
    `,
  },
});
