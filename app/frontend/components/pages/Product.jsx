import React from 'react';
import Relay from 'react-relay';
import { Link } from 'react-router';
import Colors from 'material-ui/lib/styles/colors';
import { StyleResizable } from 'material-ui/lib/mixins';
import Collapse from '../Collapse';
import ProductVendor from './ProductVendor';
import stringTemplate from 'string-template';
import ImageGallery from '../ImageGallery';
import Helmet from 'react-helmet';

const Product = React.createClass({

  displayName: 'Product',

  mixins: [StyleResizable],

  propTypes: {
    viewer: React.PropTypes.object,
  },

  contextTypes: {
    muiTheme: React.PropTypes.object,
    router: React.PropTypes.object.isRequired,
  },

  getInitialState() {
    return {
    };
  },

  getStyles() {
    const {
      muiTheme: {
        rawTheme: {
          palette,
        },
      },
    } = this.context;

    return {
      root: {
        maxWidth: '1222',
        margin: '0 auto',
      },
      container: {
        border: '1px ' + Colors.faintBlack + ' solid',
        backgroundColor: '#fff',
        margin: 10,
      },
      padding: {
        padding: '15px',
      },
      inlineBlock: {
        display: 'inline-block',
      },
      headerTextDefault: {
        borderRight: '1px ' + Colors.faintBlack + ' solid',
      },
      headerTextActive: {
        color: palette.accent1Color,
      },
      slider: {
        maxWidth: 500,
        margin: '0 auto',
      },
      collapse: {
        textAlign: 'left',
        marginTop: 25,
      },
      collapseContent: {
        padding: 15,
      },
    };
  },

  iterateCategories(arr, parentId) {
    let leafs = true;
    arr.forEach(ele => {
      if (ele.parentId === parentId) {
        leafs = false;
        ele['leaf'] = this.iterateCategories(arr, ele.cid);
      }
    });
    return leafs;
  },

  _getProductImages(product) {
    if (product.images && product.images.length > 0) {
      return product.images.map((image) => ({
        original: stringTemplate(product.largeImageUrlPrefix, [image]),
        thumbnail: stringTemplate(product.mediumImageUrlPrefix, [image]),
        originalAlt: `${product.brandName} ${product.modelNumber}`,
      }));
    } else {
      return [];
    }
  },

  render() {

    const {product} = this.props.viewer;

    let thStyle = 'style="width: 33%; vertical-align: top;"';
    let tdStyle = 'style="vertical-align: top;"';
    let specs = '';
    specs = '<table><tbody>';
    JSON.parse(product.specs).forEach(ele => {
        specs += `<tr><th ${thStyle}>${ele.name}</th><td ${tdStyle}>${ele.value}</td></tr>`;
    });
    specs += '</table></tbody>';

    const styles = this.getStyles();
    const categoryTree = product.categoryPath;
    this.iterateCategories(categoryTree, categoryTree[0].cid);

    const isScreenSmall = !this.isDeviceSize(StyleResizable.statics.Sizes.MEDIUM);

    return (
      <div style={styles.root}>
        <Helmet
          htmlAttributes={{lang: 'zh'}}
          title={product.name}
          titleTemplate="pvp9.com - %s"
          defaultTitle="Product detail"
          meta={[
            {name: 'description', content: `${product.name}`},
          ]} />
        <div style={styles.container}>
          {categoryTree.map((ele, i) => {
            if (!ele.leaf && ele.parentId) {
              return (
                <Link
                  key={'_category' + ele.cid}
                  to={`/category/${ele.cid}`}
                  activeStyle={styles.headerTextActive}
                  style={Object.assign({}, styles.padding, styles.inlineBlock, styles.headerTextDefault)}>
                  {ele.ename || ele.cname}
                </Link>
              );
            }
          })}
        </div>
        <div style={styles.container}>
          <img style={styles.padding} src={product.brandIconUrl} />
          <center>
            <h3 style={styles.padding}>{product.name}</h3>
          </center>
          <ImageGallery
            items={this._getProductImages(product)}
            showNav={isScreenSmall}
            showThumbnails={!isScreenSmall}
            server={true}
            autoPlay={true}
            slideInterval={4000}/>
          <Collapse
            rootStyle={styles.collapse}
            insertHTML={true}
            title="Details & Specs" >
            {
              `Brand: ${product.brandName} <br>` +
              `Model: ${product.modelNumber} <br>` +
              `Categories: ${
                categoryTree.reduce((prev, next) => {
                  if (next.leaf) {
                    prev.push(next.ename || next.cname);
                  }
                  return prev;
                }, [])}<br>` +
              `Specifications: ${specs}` +
              `Product Info: ${product.description}`
            }
          </Collapse>
        </div>

        {product.vendorProducts.map(ele => (
          <div key={'_vendors' + ele.id} style={styles.container}>
            <ProductVendor vendorProduct={ele} />
          </div>
        ))}
      </div>
    );
  },

});

module.exports = Relay.createContainer(Product, {
  initialVariables: {
    pid: null,
  },

  fragments: {
    viewer: (variables) => Relay.QL`
      fragment on Viewer {
        product(pid: $pid) {
          name,
          description,
          brandName,
          brandIconUrl,
          modelNumber,
          mediumImageUrlPrefix,
          largeImageUrlPrefix,
          averageSalesDaily,
          specs,
          images,
          categoryPath {
            cid,
            ename,
            cname,
            parentId
          },
          vendorProducts {
            id,
            ${ProductVendor.getFragment('vendorProduct')}
          }
        }
      }
    `,
  },
});
