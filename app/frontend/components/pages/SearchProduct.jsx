import React from 'react';
import Relay from 'react-relay';
import { Link } from 'react-router';
import Colors from 'material-ui/lib/styles/colors';
import InfiniteScroll from '../InfiniteScroll';
import SelectField from 'material-ui/lib/select-field';
import MenuItem from 'material-ui/lib/menus/menu-item';
import ResponsiveGroup from '../ResponsiveGroup';
import ProductItem from './ProductItem';
import Helmet from 'react-helmet';

const ProductSearch = React.createClass({

  displayName: 'ProductSearch',

  propTypes: {
    viewer: React.PropTypes.object,
    params: React.PropTypes.object,
    relay: React.PropTypes.any,
  },

  contextTypes: {
    muiTheme: React.PropTypes.object,
  },

  getInitialState() {
    return {
      isLoading: false,
    };
  },

  getStyles() {
    const {
      muiTheme: {
        rawTheme: {
          palette,
        },
      },
    } = this.context;

    return {
      root: {
        maxWidth: '1222',
        margin: '0 auto',
      },
      container: {
        border: '1px ' + Colors.faintBlack + ' solid',
        backgroundColor: '#fff',
        margin: 10,
      },
      padding: {
        padding: '15px',
      },
      inlineBlock: {
        display: 'inline-block',
      },
      headerTextDefault: {
        borderRight: '1px ' + Colors.faintBlack + ' solid',
      },
      headerTextActive: {
        color: palette.accent1Color,
      },
      productItem: {
        display: 'block',
        overflow: 'hidden',
        width: 200,
        padding: '15px 10px 10px 10px',
        borderTop: '1px ' + Colors.faintBlack + ' solid',
      },
    };
  },

  _handleOrderByChange(event, index, value) {
    if (!this.state.isLoading) {
      this.setState({isLoading: true}, () => {
        this.props.relay.setVariables({
          count: 12,
          orderBy: value,
        }, (readyState) => {
          if (readyState.done) {
            this.setState({isLoading: false});
          }
        });
      });
    }
  },

  _handleLoadMore() {
    if (!this.state.isLoading) {
      this.setState({isLoading: true}, () => {
        this.props.relay.setVariables({
          count: this.props.relay.variables.count + 8,
        }, (readyState) => {
          if (readyState.done) {
            this.setState({isLoading: false});
          }
        });
      });
    }
  },

  render() {
    const { products } = this.props.viewer;
    const { params } = this.props;
    const styles = this.getStyles();

    return (
      <div style={styles.root}>
        <Helmet
          htmlAttributes={{lang: 'zh'}}
          title={params.query}
          titleTemplate="pvp9.com - %s"
          defaultTitle="Search result"
          meta={[
            {name: 'description', content: params.query},
          ]} />
        <div style={styles.container}>
          <div style={Object.assign({}, styles.padding, styles.inlineBlock, styles.headerTextActive)}>{params.query}</div>
        </div>
        <div style={styles.container}>
          <div style={styles.padding}>
            <SelectField
              value={this.props.relay.variables.orderBy}
              onChange={this._handleOrderByChange}>
              <MenuItem value="SALES_HIGH_TO_LOW" primaryText="sales: high to low"/>
              <MenuItem value="SALES_LOW_TO_HIGH" primaryText="sales: low to high"/>
              <MenuItem value="PRICE_LOW_TO_HIGH" primaryText="price: low to high"/>
              <MenuItem value="PRICE_HIGH_TO_LOW" primaryText="price: high to low"/>
            </SelectField>
          </div>
          <InfiniteScroll
            hasMore={products.pageInfo.hasNextPage}
            loadMore={this._handleLoadMore}>
            <ResponsiveGroup
              autoOrient={true}
              defaultChildrenHeight={340}>
              {products.edges.map(({node: product}) => (
                <ProductItem
                  key={'_product' + product.upc}
                  containerElement={<Link to={`/product/${product.upc}`} />}
                  product={product}
                  style={styles.productItem} />
              ))}
            </ResponsiveGroup>
          </InfiniteScroll>
        </div>
      </div>
    );
  },

});

module.exports = Relay.createContainer(ProductSearch, {
  initialVariables: {
    query: null,
    orderBy: 'SALES_HIGH_TO_LOW',
    count: 12,
  },

  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        products(first: $count, query: $query, orderBy: $orderBy) {
          edges {
            node {
              upc,
              ${ProductItem.getFragment('product')}
            }
          },
          pageInfo {
            hasNextPage
          }
        }
      }
    `,
  },
});
