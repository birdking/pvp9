import React from 'react';
import Relay from 'react-relay';
import AddCategoryMutation from '../../mutation/AddCategoryMutation';
import {
  Dialog,
  Table,
  TableBody,
  TableFooter,
  TableHeader,
  TableRow,
  TableHeaderColumn,
  TableRowColumn,
  TextField,
  FlatButton,
  RaisedButton,
  SelectField,
  MenuItem,
} from 'material-ui';

const CategoryList = React.createClass({

  displayName: 'CategoryList',

  propTypes: {
    viewer: React.PropTypes.object,
  },

  contextTypes: {
    muiTheme: React.PropTypes.object,
    router: React.PropTypes.object.isRequired,
  },

  getInitialState() {
    return {
      isActiveValue: true,
      addDialogOpenFlag: false,
    };
  },

  _onAddCategory(e) {
    e.preventDefault();
    this.setState({
      addDialogOpenFlag: true,
    });
  },

  _onHandleChange(event, index, isActiveValue) {
    this.setState({isActiveValue});
  },

  _onDialogSubmit() {
    // if (this._checkAllEmpty()) {

    let cname = this.refs.cname.getValue();
    let ename = this.refs.ename.getValue();
    let isActive = this.state.isActiveValue;
    let parentId = this.refs.parentId.getValue();

    Relay.Store.commitUpdate(
      new AddCategoryMutation({
        cname,
        ename,
        isActive,
        parentId,
        viewer: this.props.viewer,
      })
    );
    this.setState({
      addDialogOpenFlag: false,
    });
    // }
  },

  _onDialogRequestClose() {
    this.setState({
      addDialogOpenFlag: false,
    });
  },

  getStyles(){
    let styles = {
      textField: {
        margin: '20px',
      },
    };
    return styles;
  },

  render() {
    const styles = this.getStyles();
    const { categories } = this.props.viewer;
    // const {detail_count} = this.props.businessEntity;
    const standardActions = [
      <FlatButton
        label="取消"
        secondary={true}
        onTouchTap={this._onDialogRequestClose}
      />,
      <FlatButton
        label="添加"
        primary={true}
        onTouchTap={this._onDialogSubmit}
      />,
    ];


    return (
      <div>
        <Dialog
          ref="addDialog"
          title={'添加新的分类'}
          open={this.state.addDialogOpenFlag}
          onRequestClose={this._onDialogRequestClose}
          actions={standardActions}>
          <TextField
            ref="cname"
            style={styles.textField}
            hintText="分类中文名称"
            floatingLabelText="输入分类名称"
          />
          <TextField
            ref="ename"
            style={styles.textField}
            hintText="分类英文名称"
            floatingLabelText="输入分类英文名称"
          />
          <SelectField
            ref="isActive"
            style={styles.textField}
            value={this.state.isActiveValue}
            onChange={this._onHandleChange}>
            <MenuItem value={true} primaryText="分类激活: TRUE"/>
            <MenuItem value={false} primaryText="分类激活: FALSE"/>
          </SelectField>
          <TextField
            ref="parentId"
            style={styles.textField}
            hintText="分类父ID"
            floatingLabelText="输入分类父ID"
          />
        </Dialog>
        <RaisedButton
          label="ADD CATEGORY"
          primary={true}
          onTouchTap={this._onAddCategory}
        />
        <Table selectable={false}>
          <TableHeader
            adjustForCheckbox={false}
            displaySelectAll={false}>
            <TableRow>
              <TableHeaderColumn colSpan="6" style={{textAlign: 'center'}}>
                分类列表
              </TableHeaderColumn>
            </TableRow>
            <TableRow>
              <TableHeaderColumn>ID</TableHeaderColumn>
              <TableHeaderColumn>中文名称</TableHeaderColumn>
              <TableHeaderColumn>英文名称</TableHeaderColumn>
              <TableHeaderColumn>是否激活</TableHeaderColumn>
              <TableHeaderColumn>父ID</TableHeaderColumn>
              <TableHeaderColumn>货物数量</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody
            displayRowCheckbox={false}
            showRowHover={true}>
            {categories.edges.map((ele) => (
              <TableRow key={'_detail' + ele.node.cid}>
                <TableRowColumn>{ele.node.cid}</TableRowColumn>
                <TableRowColumn>{ele.node.cname}</TableRowColumn>
                <TableRowColumn>{ele.node.ename}</TableRowColumn>
                <TableRowColumn>{ele.node.isActive ? '是' : '否'}</TableRowColumn>
                <TableRowColumn>{ele.node.parentId || '无父'}</TableRowColumn>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
    );
  },


});


// module.exports = CategoryList;

module.exports = Relay.createContainer(CategoryList, {
  initialVariables: {
    page: 0,
    limit: 12,
  },

  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        categories (first: 100) {
          edges {
            node {
              cid,
              cname,
              ename,
              isActive,
              parentId
            }
          }
        }
        ${AddCategoryMutation.getFragment('viewer')},
      }
    `,
  },
});
