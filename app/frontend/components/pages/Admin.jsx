import React from 'react';
import PageWithNav from './PageWithNav';

const Admin = React.createClass({

  displayName: 'Admin',

  propTypes: {
    children: React.PropTypes.node,
    location: React.PropTypes.object,
  },

  render() {
    const { location } = this.props;
    const menuItems = [
      { route: '/admin/categoryList', text: '分类列表'},
      { route: '/admin/categoryMappingList', text: '分类Mapping列表'},
    ];

    return (
      <PageWithNav
        location={this.props.location}
        menuItems={menuItems}>
      {this.props.children}
      </PageWithNav>
    );
  },

});

module.exports = Admin;
