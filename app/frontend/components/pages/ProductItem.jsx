import React from 'react';
import Relay from 'react-relay';
import ResponsiveGroup from '../ResponsiveGroup';
import EnhancedButton from 'material-ui/lib/enhanced-button';
import Colors from 'material-ui/lib/styles/colors';
import stringTemplate from 'string-template';
import RatingStar from '../RatingStar';

const ProductItem = React.createClass({

  displayName: 'ProductItem',

  propTypes: {
    product: React.PropTypes.object.isRequired,
    style: React.PropTypes.object,
  },

  getStyles() {
    return {
      productName: {
        wordWrap: 'break-word',
        lineHeight: '20px',
        height: 60,
        margin: '6px 0',
        textAlign: 'center',
        overflow: 'hidden',
      },
      productSummary: {
        width: '75%',
      },
      productImage: {
        width: '25%',
      },
      textRight: {
        textAlign: 'right',
      },
      priceDisable: {
        textDecoration: 'line-through',
        color: Colors.grey700,
        marginRight: 5,
      },
      priceNormal: {
        fontSize: 14,
        fontWeight: 'bold',
        color: Colors.lightBlack,
      },
      priceHighlight: {
        fontWeight: 'bold',
        fontSize: 16,
        color: Colors.red700,
      },
      ratingImage: {
        marginTop: 5,
        width: 20,
        heiht: 20,
      },
    };
  },

  render() {
    const {
      product,
      ...other,
    } = this.props;
    const productImage = product.images && product.images.length > 0 ? stringTemplate(product.mediumImageUrlPrefix, [product.images[0]]) : '//www.sewcurvy.com/siteimages/_templates/no-image.jpg';
    const styles = this.getStyles();

    return (
      <EnhancedButton linkButton={true} {...other}>
        <ResponsiveGroup
          autoOrient={true}
          orientOnSmallScreen="horizontal">
          <img
            src={productImage}
            style={styles.productImage}/>
          <div style={styles.productSummary}>
            <div style={styles.productName}>{product.name}</div>
            <div style={styles.textRight}>
              <RatingStar rating={product.customerRating}/>
            </div>
            <div style={styles.textRight}>
              {product.isProductOnSale &&
                <span style={styles.priceDisable}>{product.regularPrice}</span>
              }
              <span style={product.isProductOnSale ? styles.priceHighlight : styles.priceNormal}>{product.salePrice}</span>
            </div>
          </div>
        </ResponsiveGroup>
      </EnhancedButton>
    );
  },

});

module.exports = Relay.createContainer(ProductItem, {
  fragments: {
    product: () => Relay.QL`
      fragment on Product {
        name,
        averageSalesDaily,
        mediumImageUrlPrefix,
        isProductOnSale,
        regularPrice,
        salePrice,
        customerRating,
        images
      }
    `,
  },
});
