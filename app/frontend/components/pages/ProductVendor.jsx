import React from 'react';
import Relay from 'react-relay';
import Table from 'material-ui/lib/table/table';
import TableHeaderColumn from 'material-ui/lib/table/table-header-column';
import TableRow from 'material-ui/lib/table/table-row';
import TableHeader from 'material-ui/lib/table/table-header';
import TableRowColumn from 'material-ui/lib/table/table-row-column';
import TableBody from 'material-ui/lib/table/table-body';
import StoreDetailDialog from './StoreDetailDialog';
import Colors from 'material-ui/lib/styles/colors';
import RatingStar from '../RatingStar';
import IconButton from 'material-ui/lib/icon-button';
import DoubleDown from '../svg-icons/DoubleDown';
import EnhancedButton from 'material-ui/lib/enhanced-button';
import RaisedButton from 'material-ui/lib/raised-button';

const ProductVendor = React.createClass({

  displayName: 'ProductVendor',

  propTypes: {
    vendorProduct: React.PropTypes.object,
    style: React.PropTypes.object,
    relay: React.PropTypes.any,
  },

  getInitialState() {
    return {
      isLoading: false,
      storeDialogOpen: false,
      storeData: {},
      displayQuantity: 0,
    };
  },

  styles: {
    priceDisable: {
      textDecoration: 'line-through',
      color: Colors.grey700,
      marginRight: 5,
    },
    priceNormal: {
      fontSize: 14,
      fontWeight: 'bold',
      color: Colors.lightBlack,
    },
    priceHighlight: {
      fontWeight: 'bold',
      fontSize: 16,
      color: Colors.red700,
    },
    vendorImg: {
      marginTop: 2,
      height: 54,
    },
    tableHeader: {
      minWidth: '600px',
      border: 'none',
    },
    fullWidth: {
      width: '100%',
      height: '100%',
    },
    nonPadding: {
      padding: 0,
    },
  },
  _getStoreDetail(ele, displayQuantity) {
    // this.refs.storeDia.props.open = true;
    this.setState({
      storeDialogOpen: true,
      storeData: ele,
      displayQuantity: displayQuantity,
    });
  },

  _onRequestClose() {
    this.setState({
      storeDialogOpen: false,
    });
  },

  _getMoreStore() {
    if (!this.state.isLoading) {
      this.setState({isLoading: true}, () => {
        this.props.relay.setVariables({
          count: this.props.relay.variables.count + 5,
        }, (readyState) => {
          if (readyState.done) {
            this.setState({isLoading: false});
          }
        });
      });
    }
  },

  render() {
    let {
      vendorProduct,
      ...other,
    } = this.props;

    const shippingQuantity = vendorProduct.shipping && vendorProduct.shipping.purchasable ? (vendorProduct.shipping.quantity > 0 ? vendorProduct.shipping.quantity : '1+') : 0;
    const hasStore = vendorProduct.pickups.edges && vendorProduct.pickups.edges.length > 0;

    return (
      <div {...other}>
        <StoreDetailDialog
          store={this.state.storeData}
          displayQuantity={this.state.displayQuantity}
          onRequestClose={this._onRequestClose}
          open={this.state.storeDialogOpen}/>
        <Table>
          <TableHeader
            style={this.styles.tableHeader}
            adjustForCheckbox={false}
            displaySelectAll={false}>
            <TableRow displayBorder={false}>
              <TableHeaderColumn colSpan="3">
                <img style={this.styles.vendorImg} src={vendorProduct.vendor.iconUrl} />
              </TableHeaderColumn>
              <TableHeaderColumn colSpan="3">
                {vendorProduct.isProductOnSale &&
                  <span style={this.styles.priceDisable}>{vendorProduct.regularPrice}</span>
                }
                <span style={vendorProduct.isProductOnSale ? this.styles.priceHighlight : this.styles.priceNormal}>{vendorProduct.salePrice}</span>
              </TableHeaderColumn>
              <TableHeaderColumn colSpan="2">
                <RatingStar rating={vendorProduct.customerRating} />
              </TableHeaderColumn>
              <TableHeaderColumn colSpan="2">
                <RaisedButton
                  linkButton={true}
                  href={vendorProduct.sourceUrl}
                  label={'Online Shipping: ' + shippingQuantity} primary={true} disabled={!shippingQuantity} />
              </TableHeaderColumn>
            </TableRow>
            <TableRow displayBorder={false} style={ hasStore ? {} : {display: 'none'}}>
              <TableHeaderColumn colSpan="2">City</TableHeaderColumn>
              <TableHeaderColumn colSpan="4">Address</TableHeaderColumn>
              <TableHeaderColumn colSpan="2">Distance(km)</TableHeaderColumn>
              <TableHeaderColumn colSpan="2">Stock</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {vendorProduct.pickups.edges.map(({node: ele}) => {
              let pickupQuantity = ele.hasInventory ? (ele.quantity > 0 ? ele.quantity : '1+') : 0;
              return (
                <TableRow
                  key={'_store' + ele.store.sid}
                  selectable={false}
                  onTouchTap={() => this._getStoreDetail(ele.store, pickupQuantity)}>
                  <TableRowColumn colSpan="2">{ele.store.city}</TableRowColumn>
                  <TableRowColumn colSpan="4">{ele.store.address}</TableRowColumn>
                  <TableRowColumn colSpan="2">{ele.distance}</TableRowColumn>
                  <TableRowColumn colSpan="2">{pickupQuantity}</TableRowColumn>
                </TableRow>
              )
            })}
            {vendorProduct.pickups.pageInfo.hasNextPage &&
              <TableRow selectable={false}>
                <TableRowColumn colSpan="10" style={this.styles.nonPadding}>
                  <EnhancedButton onTouchTap={this._getMoreStore} style={Object.assign({}, this.styles.fullWidth, this.styles.nonPadding)}>
                    <DoubleDown color={Colors.minBlack} />
                  </EnhancedButton>
                </TableRowColumn>
              </TableRow>
            }
          </TableBody>
        </Table>
      </div>
    );
  },
});

module.exports = Relay.createContainer(ProductVendor, {
  initialVariables: {
    count: 5,
  },

  fragments: {
    vendorProduct: () => Relay.QL`
      fragment on VendorProduct {
        id,
        vendor {
          name,
          iconUrl
        },
        regularPrice,
        salePrice,
        isProductOnSale,
        customerRating,
        sourceUrl,
        shipping {
          purchasable,
          quantity
        },
        pickups(first: $count) {
          edges {
            node {
              id,
              hasInventory,
              quantity,
              distance,
              store {
                sid,
                name,
                address,
                city,
                state,
                country,
                postalCode,
                phone,
                hoursList {
                  id,
                  day,
                  open,
                  close,
                  isToday,
                  isOpen
                }
              }
            }
          },
          pageInfo {
            hasNextPage
          }
        }
      }
    `,
  },
});
