import React from 'react';
import Relay from 'react-relay';
import AppBar from 'material-ui/lib/app-bar';
import IconButton from 'material-ui/lib/icon-button';
import AutoComplete from 'material-ui/lib/auto-complete';
import {Spacing} from 'material-ui/lib/styles';
import {darkWhite, lightWhite, grey900, pink400} from 'material-ui/lib/styles/colors';
// import AppLeftNav from './AppLeftNav';
import FullWidthSection from './FullWidthSection';
import ThemeManager from 'material-ui/lib/styles/theme-manager';
import themeDecorator from 'material-ui/lib/styles/theme-decorator';
import DefaultTheme from '../themes/default';
import SearchIcon from './svg-icons/Search';
import EqualizerIcon from './svg-icons/Equalizer';
import Helmet from 'react-helmet';
// import { FormattedRelative } from 'react-intl';
// import { IntlProvider } from 'react-intl';

const Master = React.createClass({

  displayName: 'Master',

  propTypes: {
    viewer: React.PropTypes.object,
    children: React.PropTypes.node,
    relay: React.PropTypes.any,
  },

  contextTypes: {
    router: React.PropTypes.object.isRequired,
    muiTheme: React.PropTypes.object,
  },

  getStyles() {
    const {
      muiTheme: {
        rawTheme: {
          palette,
        },
        zIndex: {
          appBar,
        },
      },
    } = this.context;

    const styles = {
      root: {
        paddingTop: Spacing.desktopKeylineIncrement,
        minHeight: 400,
      },
      appBar: {
        position: 'fixed',
        zIndex: appBar + 1,
        top: 0,
      },
      inputText: {
        color: palette.accent1Color,
        fontWeight: 'bold',
        letterSpacing: 1,
      },
      footer: {
        textAlign: 'center',
      },
      a: {
        color: palette.accent1Color,
      },
      p: {
        margin: '0 auto',
        padding: 0,
        color: palette.secondaryTextColor,
        maxWidth: 335,
      },
      underlineStyle: {
        borderColor: 'transparent',
      },
      underlineFocusStyle: {
        borderColor: palette.accent1Color,
      },
    };

    return styles;
  },

  handleTouchTapLeftIconButton() {
    this.context.router.push({pathname: '/'});
  },

  handleTouchTapRightIconButton() {
    const { searchInput } = this.refs;
    if (searchInput) {
      const { searchTextField } = searchInput.refs;
      if (searchTextField) {
        const searchText = searchInput.getValue();
        if (searchText && searchText.length > 0) {
          this.context.router.push({pathname: `/search/${searchText}`});
          searchTextField.blur();
        } else {
          searchTextField.focus();
        }
      }
    }
  },

  _onUpdateInput(searchText, dataSource) {
    const { searchInput } = this.refs;
    setTimeout(() =>{
      const currentText = searchInput.getValue();
      if (searchText === currentText) {
        this.props.relay.setVariables({
          searchText: searchText,
        });
      }
    }, 500);
  },

  _getDataSource(autoComplete) {
    return autoComplete.map(({word}) => word);
  },

  render() {
    const {
      viewer,
      children,
    } = this.props;

    const {
      muiTheme: {
        rawTheme: {
          palette,
        },
        prepareStyles,
      },
    } = this.context;

    const styles = this.getStyles();

    const postDate    = Date.now() - (1000 * 60 * 60 * 24);
    const commentDate = Date.now() - (1000 * 60 * 60 * 2);
    const meetingDate = Date.now() + (1000 * 60 * 51);

    return (
      <div style={styles.root}>
        <Helmet
          htmlAttributes={{lang: 'zh'}}
          title="Categories"
          titleTemplate="pvp9.com - %s"
          defaultTitle="Categories of products"
          meta={[
            {name: 'description', content: 'Categories of products'},
          ]} />
        <AppBar
          zDepth={2}
          style={styles.appBar}
          iconElementLeft={
            <IconButton
              onTouchTap={this.handleTouchTapLeftIconButton}
              tooltip="HOME">
              <EqualizerIcon />
            </IconButton>
          }
          iconElementRight={
            <IconButton
              onTouchTap={this.handleTouchTapRightIconButton}
              tooltip="Search">
              <SearchIcon color="white" />
            </IconButton>
          }
          title={
            <AutoComplete
              disableFocusRipple={false}
              ref="searchInput"
              filter={AutoComplete.fuzzyFilter}
              dataSource={this._getDataSource(viewer.autoComplete)}
              onUpdateInput={this._onUpdateInput}
              underlineStyle={styles.underlineStyle}
              underlineFocusStyle={styles.underlineFocusStyle}
              onNewRequest={this.handleTouchTapRightIconButton}
              fullWidth={true}
              inputStyle={styles.inputText} />
          } />
          {children}
        <FullWidthSection style={styles.footer}>
          <p style={prepareStyles(styles.p)}>
            {'CopyRight © 2016 - '}
            <a style={styles.a} href="#">
              pvp9.com
            </a>
          </p>
        </FullWidthSection>
      </div>
    );
  },
});

export function prepareMasterParams(params, route) {
  return {
    ...params,
    lang: params.lang || route.defaultLang,
  };
};

export default Relay.createContainer(themeDecorator(ThemeManager.getMuiTheme(DefaultTheme, { userAgent: 'all'}))(Master), {
  initialVariables: {
    lang: null,
    searchText: null,
    limit: 10,
  },

  fragments: {
    viewer: () => Relay.QL`
      fragment on Viewer {
        autoComplete (searchText: $searchText, limit: $limit) {
          word
        }
      }
    `,
  },
});
