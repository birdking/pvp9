import React from 'react';

export default class RatingStar extends React.Component {

  static displayName = 'RatingStar'

  static propTypes = {
    style: React.PropTypes.object,
    rating: React.PropTypes.number,
  }

  constructor(props) {
    super(props);
  }

  getStyles() {
    return {
      starBackground: {
        display: 'inline-block',
        margin: '6px 0',
        width: 98,
        height: 18,
        background: 'url(/images/sprite-rating-stars.png) -4px -28px no-repeat',
      },
      starForeground: {
        height: 18,
        background: 'url(/images/sprite-rating-stars.png) -4px -4px no-repeat',
      },
    };
  }

  render() {
    const { rating } = this.props;
    const styles = this.getStyles();

    return (
      <div style={styles.starBackground}>
        <div style={Object.assign({}, styles.starForeground, {width: rating*20})} />
      </div>
    );
  }

}
