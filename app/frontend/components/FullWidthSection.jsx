import React from 'react';
import Clearfix from 'material-ui/lib/clearfix';
import Mixins from 'material-ui/lib/mixins';
import { Spacing, Typography } from 'material-ui/lib/styles';
const {StylePropable, StyleResizable} = Mixins;
const DesktopGutter = Spacing.desktopGutter;


const FullWidthSection = React.createClass({

  displayName: FullWidthSection,

  propTypes: {
    contentStyle: React.PropTypes.object,
    contentType: React.PropTypes.string,
    style: React.PropTypes.object,
    useContent: React.PropTypes.bool,
    children: React.PropTypes.node,
  },

  mixins: [StylePropable, StyleResizable],

  getDefaultProps() {
    return {
      useContent: false,
      contentType: 'div',
    };
  },

  getStyles() {
    return  {
      root: {
        padding: DesktopGutter + 'px',
        boxSizing: 'border-box',
      },
      content: {
          maxWidth: '1200px',
          margin: '0 auto',
      },
      rootWhenSmall: {
          paddingTop: (DesktopGutter * 2) + 'px',
          paddingBottom: (DesktopGutter * 2) + 'px',
      },
      rootWhenLarge: {
          paddingTop: (DesktopGutter * 3) + 'px',
          paddingBottom: (DesktopGutter * 3) + 'px',
      },
    };
  },

  render() {
    let {
      style,
      useContent,
      contentType,
      contentStyle,
      ...other,
    } = this.props;

    let styles = this.getStyles();

    let content;
    if (useContent) {
      content =
        React.createElement(
          contentType,
          {style: this.mergeStyles(styles.content, contentStyle)},
          this.props.children,
        );
    } else {
      content = this.props.children;
    }

    return (
      <Clearfix {...other}
        style={this.mergeStyles(
          styles.root,
          this.isDeviceSize(StyleResizable.statics.Sizes.SMALL) && styles.rootWhenSmall,
          this.isDeviceSize(StyleResizable.statics.Sizes.LARGE) && styles.rootWhenLarge,
          style
        )}>
        {content}
      </Clearfix>
    );
  },
});

module.exports = FullWidthSection;
