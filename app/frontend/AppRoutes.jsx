import React from 'react';
import Relay from 'react-relay';
import {Redirect, IndexRoute, IndexRedirect, Router, Route, Link} from 'react-router';
import Master, { prepareMasterParams } from './components/Master';
import SearchProduct from './components/pages/SearchProduct';
import CategoryProduct from './components/pages/CategoryProduct';
import Product from './components/pages/Product';

const CategoryListQueries = {
  viewer: () => Relay.QL`query { viewer }`,
};

export default (lang) => {
  return (
    <Route
      path="/"
      component={Master}
      queries={CategoryListQueries}
      stateParams={['lang']}
      defaultLang={lang}
      prepareParams={prepareMasterParams}>
      <IndexRedirect to="/category/1" />
      <Route
        path="category/:cid"
        component={CategoryProduct}
        queries={CategoryListQueries}
        prepareParams={CategoryProduct.prepareParams} />
      <Route
        path="search/:query"
        component={SearchProduct}
        queries={CategoryListQueries} />
      <Route
        path="product/:pid"
        component={Product}
        queries={CategoryListQueries} />
    </Route>
  );
};
