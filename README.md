# API Endpoints:

Top level: http://www.bestbuy.ca/api/v2/json/category/Departments

- Product API:
- Products for all categories: http://www.bestbuy.ca/api/v2/json/search?categoryid=departments
- Product list for category: http://www.bestbuy.ca/api/v2/json/search?categoryid={catid}&query={text} (e.g. 20001)
- Product details: http://www.bestbuy.ca/api/v2/json/product/{sku} (e.g. 10384058)
- Location list: http://www.bestbuy.ca/api/v2/json/locations?postalcode={postalcode} (e.g. v6x1a8)
- Location detail: http://www.bestbuy.ca/api/v2/json/locations/{locationKey} (e.g. 228)
- Product reviews: http://www.bestbuy.ca/api/v2/json/reviews/{sku} (e.g. 10384058)
- Product availability: http://api.bestbuy.ca/availability/products?accept-language=en&skus=10403904&accept=application%2Fvnd.bestbuy.standardproduct.v1%2Bjson&postalCode=V6X1A8&locations=228%7C941%7C850%7C852%7C853%7C854%7C994%7C952%7C973%7C227%7C825%7C705%7C242%7C826%7C829%7C13%7C212%7C600%7C216%7C961%7C226%7C958%7C241%7C701%7C929

- Image url role: http://multimedia.bbycastatic.ca/multimedia/products/500x500/m21/m2122/m2122083.jpg  
    http://multimedia.bbycastatic.ca/multimedia/products/{size}/{[,3]}/{[,5]}/{sku}.jpg
    size=[55x55, 100x100, 150x150, 250x250, 300x300, 400x400, 500x500]

```js
{"configuration": {
"config-url": "http://www.bestbuy.ca/ns/mobile/android/configuration/v1.3.0/en",
	"latest-app-version": "1.4.3",
	"latest-app-alert": "true",
	"latest-app-alert-message": "There is a new version of BestBuy available from the Play Store",
	"latest-app-url": "https://play.google.com/store/apps/details?id=com.coppi.bestbuy&feature=search_result#?t=W251bGwsMSwxLDEsImNvbS5jb3BwaS5iZXN0YnV5Il0.",
    "app-closed-start-time": "2015-11-30T02:01:00Z",
    "app-closed-end-time": "2015-11-30T04:00:00Z",
    "app-closed-title": "Our Cyber Monday Sale starts at 12:01 A.M. ET. Get ready to save.",
    "app-closed-message": "",
    "app-closed-button-text": "Get a sneak peek!",
    "app-closed-url": "http://flyer.bestbuy.ca/flyers/bestbuy-weekly?type=1&locale=EN&auto_locate=true&flyer_run_id=92861",
	"checkout-delete-cookie-name": "ASP.NET_SessionId",
	"checkout-delete-cookie-domain": ".bestbuy.ca",
	"store-proximity": "200",
	"store-max-display-count": "3",
	"checkout-enabled": "true",
	"checkout-url": "https://m-ssl.bestbuy.ca/order/addtocart.ashx?pcname=MCCPCatalog&lang=en&removecurrentitems=true",
	"checkout-disabled-message": "Browse our DEALS now! Checkout opens at 12:01 A.M. ET.",
	"category-api-url": "http://www.bestbuy.ca/api/v2/json/category/",
	"product-api-url": "http://www.bestbuy.ca/api/v2/json/product/",
	"search-api-url": "http://www.bestbuy.ca/api/v2/json/search",
	"store-api-url": "http://www.bestbuy.ca/api/v2/json/locations",
	"pdp-api-url": "http://www.bestbuy.ca/api/v2/json/product/",
	"reviews-api-url": "http://www.bestbuy.ca/api/v2/json/reviews/",
	"availability-api-url": "http://www.bestbuy.ca/api/v2/json/availability",
	"publication-api-url": "http://www.bestbuy.ca/api/v2/json/publication/",
	"loading-screen-timeout": "15",
	"main-feature-url":"http://www.bestbuy.ca/Projects/AndroidPromotions/mainfeature.aspx?lang=en-CA",
	"availability-pdp-proximity": "30",
	"availability-cart-proximity": "200",
	"availability-pdp-pagesize": "10",
	"availability-pdp-instore-enabled": "true",
	"media-base-url": "https://multimedia.bbycastatic.ca",
	"pdp-message-text": "Free shipping on orders over $25",
	"onsale-filter": "Current Offers|On Sale",
	"category-icon-base-url" :"https://multimedia.bbycastatic.ca/images/android/",
	"category-icon-last-update-time" :"2011-02-16T11:00:00Z",
	"location-cache-time": "30",
	"scan-history-max-count":"25",
	"onsale-enabled":"true",
	"flyer-enabled":"true",
	"flyer-browser-enabled":"false",
	"flyer-browser-url":"",
	"reserve-availability-api-url":"http://www.bestbuy.ca/api/v2/json/locations/availability",
	"reserve-enabled":"true",
	"paypal-hidden":"false",
	"paypal-disable":"false",
	"auto-complete-api-url":"http://www.bestbuy.ca/api/v2/json/search/autocomplete",

	"shipping-message-enabled":"true",
	"reserve-message-enabled":"true",
	"shipping-message-help-url":"http://m.bestbuy.ca/en-CA/help/id/HC8249.aspx",
	"reserve-message-help-url":"http://m.bestbuy.ca/en-CA/help/id/HC8250.aspx",

	"warranty-terms-conditions":"http://m.bestbuy.ca/catalog/wptermsandconditions.aspx?dmode=app&lang=en-CA",
	"warranty-manufacturer-disclosures":"http://m.bestbuy.ca/catalog/wptermsandconditions.aspx?dmode=app&lang=en-CA",
	"warranty-legal-disclosures":"http://m.bestbuy.ca/helpcustomerservice/helpdetails.aspx?HelpTitleId=HC96&lang=en-CA&dmode=app",
	"interstitial-enabled":"true",
	"warranty-enabled":"true",
	"shipping-message-display-times": [
               {"region": "AB","shippingEnd":"23:00","reserveStart": "11:00","reserveEnd":"16:00","reserveDefaultStart": "10:00","reserveDefaultEnd":"16:00"},
	       {"region": "BC","shippingEnd":"23:00","reserveStart": "11:00","reserveEnd":"16:00","reserveDefaultStart": "10:00","reserveDefaultEnd":"16:00"},
	       {"region": "MB","shippingEnd":"20:00","reserveStart": "11:00","reserveEnd":"16:00","reserveDefaultStart": "10:00","reserveDefaultEnd":"16:00"},
	       {"region": "NB","shippingEnd":"20:00","reserveStart": "12:00","reserveEnd":"16:00","reserveDefaultStart": "10:00","reserveDefaultEnd":"16:00"},   
	       {"region": "NL","shippingEnd":"20:00","reserveStart": "11:00","reserveEnd":"16:00","reserveDefaultStart": "10:00","reserveDefaultEnd":"16:00"},
	       {"region": "NT","shippingEnd":"20:00","reserveStart": "11:00","reserveEnd":"16:00","reserveDefaultStart": "10:00","reserveDefaultEnd":"16:00"},
	       {"region": "NS","shippingEnd":"20:00","reserveStart": "12:00","reserveEnd":"16:00","reserveDefaultStart": "10:00","reserveDefaultEnd":"16:00"},
	       {"region": "NU","shippingEnd":"20:00","reserveStart": "11:00","reserveEnd":"16:00","reserveDefaultStart": "10:00","reserveDefaultEnd":"16:00"},   
	       {"region": "ON","shippingEnd":"20:00","reserveStart": "11:00","reserveEnd":"16:00","reserveDefaultStart": "10:00","reserveDefaultEnd":"16:00"},
	       {"region": "PE","shippingEnd":"20:00","reserveStart": "12:00","reserveEnd":"16:00","reserveDefaultStart": "10:00","reserveDefaultEnd":"16:00"},
	       {"region": "QC","shippingEnd":"20:00","reserveStart": "11:00","reserveEnd":"16:00","reserveDefaultStart": "10:00","reserveDefaultEnd":"16:00"},
	       {"region": "SK","shippingEnd":"23:00","reserveStart": "11:00","reserveEnd":"16:00","reserveDefaultStart": "10:00","reserveDefaultEnd":"16:00"},   
	       {"region": "YT","shippingEnd":"23:00","reserveStart": "11:00","reserveEnd":"16:00","reserveDefaultStart": "10:00","reserveDefaultEnd":"16:00"}
                        ],

	"moreURLs": [
                        {"title": "Reward Zone","url": "http://www.bestbuy.ca/en-CA/reward-zone.aspx", "linkViewTarget": "browser"},
                        {"title": "Community Forums","url": "http://plug-in.bestbuy.ca" , "linkViewTarget": "currentAppAll" },
                        {"title": "Privacy Policy","url": "http://m.bestbuy.ca/en-CA/help/privacy-policy/hc1139.aspx?dmode=app" , "linkViewTarget": "currentAppAll" },
        		{"title": "Purchase Terms and Conditions","url": "http://m.bestbuy.ca/en-CA/help/terms-and-conditions/hc8140.aspx?dmode=app", "linkViewTarget": "currentAppAll"},
                        {"title": "Terms of use","url": "http://m.bestbuy.ca/static/termsofuse.aspx", "linkViewTarget": "currentAppAll"},
                        {"title": "Feedback","url": "http://m.bestbuy.ca/en-ca/androidappfeedback.aspx?dmode=app", "linkViewTarget": "currentAppAll"}
                        ]     
	}}

```


# INSTALL
[Install docker and docker-compose on Mac](https://docs.docker.com/installation/mac/)  
[Install docker on Ubuntu](http://askubuntu.com/questions/472412/how-do-i-upgrade-docker)  
[Install docker-compose on Ubuntu](https://docs.docker.com/compose/install/)



## Run docker without sudo

- Add the docker group if it doesn't already exist:  

```$ sudo groupadd docker```  

- Add the connected user "${USER}" to the docker group. Change the user name to match your preferred user:  

```$ sudo gpasswd -a ${USER} docker```  

- Restart the Docker daemon:  

```$ sudo service docker restart```  

- If you are on Ubuntu 14.04 and up use docker.io instead:  

```$ sudo service docker.io restart```  

- Either do a newgrp docker or log out/in to activate the changes to groups.  



## Can't connect to docker daemon. Is 'docker -d' running on this host?  
```$ unset DOCKER_HOST```  

------

# Sync db  
- run `./syncdb backup` once before `docker-compose up or ./tool up`  

```
  gk@gk ~/Projects/vanskyca/environment $ ./syncdb -h  


  Usage: deploy [command]

  Options:

    -h, --help             output help information

  Commands:  

    copy                 copy from remote server
    backup               backup db on remote server
```

------

# docker-compose usage  
- even we can use docker-compose to run up the enviroment,i recommend using the TOOL below instead.  
```$ cd ~/Projects/vanskyca/environment```  

- start  
```$ docker-compose up```  
- stop ``$ docker-compose stop``  
- remove all container ```$ docker-compose rm```  
- build ```$ docker-compose build```  
- help ```$ docker-compose help```  

------

# deploy  
- `docker-compose ps` to make sure vansky-web is running before deploy  
```
  gk@gk ~/Projects/vanskyca/environment $ ./www -h

  Usage: www [command]

  Options:

    -h, --help             output help information

  Commands:
    setup                run remote setup commands
    update               update deploy to the latest release
    revert [n]           revert to [n]th last deployment or 1
    curr[ent]            output current release commit
    prev[ious]           output previous release commit
    exec|run <cmd>       execute the given <cmd>
    list                 list previous deploy commits
    [ref]                deploy to [ref], the "ref" setting, or latest tag

```

------

# tool
```
  gk@gk ~/Projects/vanskyca/environment $ ./tool -h

  Usage: tool [command]

  Options:

    -h, --help             output help information

  Commands:

    crawler                log into crawler container
    postgresql             log into postgresql
    nginx                  log into vip nodejs nginx
    db                     log into db mysql container
    dbip                   show the ip of db mysql container
    up                     shorthand for docker-compose up
    rebuild                rebuild develop enviroment
    stop                   shorthand for docker-compose stop
    ps                     shorthand for docker-compose ps
    rm                     shorthand for docker-compose rm -f

```


# Port forwarding on docker-machine

### Use docker-machine info to get the name of vm. (boot2docker-vm)

set up a permanent VirtualBox NAT Port forwarding:
```

$ VBoxManage modifyvm "boot2docker-vm" --natpf1 "tcp-port8000,tcp,,8000,,8000";

```

If the vm is already running, you should run this other command:
```

$ VBoxManage controlvm "boot2docker-vm" natpf1 "tcp-port8000,tcp,,8000,,8000";

```

# Create digitalocean droplet with docker-machine

```

docker-machine create --driver digitalocean --digitalocean-access-token=d2d8ff9216a8f2f222cd81989e11c2aa430be9f8c2ac6646d99d82ac2cf3d52d --digitalocean-region=sfo1 --digitalocean-size=4gb pvp9
docker-machine create --driver generic --generic-ip-address=104.236.156.181 --generic-ssh-key ~/.ssh/id_rsa pvp9

```
