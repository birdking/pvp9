SET TIME ZONE 'UTC';
-- SET TIME ZONE 'Canada/Pacific';
CREATE EXTENSION IF NOT EXISTS pg_trgm;
CREATE EXTENSION IF NOT EXISTS address_standardizer;

DROP MATERIALIZED VIEW IF EXISTS product, word;
DROP TABLE IF EXISTS vendor, category, vendor_category, product_category,
vendor_product, vendor_product_record, vendor_store, vendor_store_hours,
vendor_product_store_pickup, vendor_product_shipping CASCADE;

DROP FUNCTION IF EXISTS get_pickup_and_shipping_sales_by_vendor_id(int) CASCADE;
DROP FUNCTION IF EXISTS get_pickup_sales_by_vendor_id(int) CASCADE;
DROP FUNCTION IF EXISTS get_shipping_sales_by_vendor_id(int) CASCADE;
DROP FUNCTION IF EXISTS get_leaf_categories_by_parent_id(int) CASCADE;
DROP FUNCTION IF EXISTS get_itself_and_children_categories_by_parent_id(int) CASCADE;
DROP FUNCTION IF EXISTS get_parent_categories_by_id(int) CASCADE;
DROP FUNCTION IF EXISTS update_product_view() CASCADE;
DROP FUNCTION IF EXISTS update_vendor_product_record_by_vendor_id(int) CASCADE;
DROP FUNCTION IF EXISTS clean_vendor_product_record_by_vendor_id(int) CASCADE;
DROP FUNCTION IF EXISTS get_all_categories_by_category_id(int) CASCADE;
DROP FUNCTION IF EXISTS get_category_path_by_upc(int) CASCADE;
DROP FUNCTION IF EXISTS update_lastmodified_column() CASCADE;
DROP FUNCTION IF EXISTS upper_trim_on_insert_update() CASCADE;
DROP FUNCTION IF EXISTS check_vendor_product_in_store(int, int) CASCADE;

CREATE OR REPLACE FUNCTION update_lastmodified_column() RETURNS trigger AS $$
BEGIN
  NEW.last_modified := NOW();
  RETURN NEW;
END;
$$ LANGUAGE PLPGSQL VOLATILE;

CREATE OR REPLACE FUNCTION upper_trim_on_insert_update() RETURNS trigger AS $$
BEGIN
    NEW.brand_name = UPPER(TRIM(NEW.brand_name));
    NEW.model_number = UPPER(TRIM(NEW.model_number));
    RETURN NEW;
END;
$$ LANGUAGE PLPGSQL VOLATILE;

CREATE OR REPLACE FUNCTION check_vendor_product_in_store(vendorProductId int, storeId int)
RETURNS boolean AS
$BODY$
BEGIN
    RETURN Exists (
      Select 1
      FROM vendor_store AS vs
      JOIN vendor_product AS vp ON vp.vendor_id = vs.vendor_id
      WHERE vp.id = vendorProductId AND vs.id = storeId
    );
END
$BODY$
LANGUAGE PLPGSQL STABLE;

/*

products: [
  {
    name,
    upc,
    category,
    vendors: [
      {
        name,
        logo,
        price,
        sku,
        shipping: {
          quantity
        },
        stores: [
          {
            name,
            address,
            hours: {
              Mon: {open, close},
              Tue: {open, close},
              ...
            },
            pickup: {
              quantity
            }
          },
          ...
        ],
      },
      ...
    ]
  },
  ...
]

*/
DROP DOMAIN IF EXISTS uint2;
CREATE DOMAIN uint2 AS int4 CHECK(VALUE >= 0 AND VALUE < 2147483647);

CREATE TABLE vendor
(
  id serial PRIMARY KEY,
  name text NOT NULL UNIQUE,
  small_image_url_prefix text NOT NULL UNIQUE,
  medium_image_url_prefix text NOT NULL UNIQUE,
  large_image_url_prefix text NOT NULL UNIQUE,
  icon_url text NOT NULL UNIQUE,
  last_modified timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
CREATE TRIGGER vendor_update_last_modified BEFORE UPDATE
ON vendor FOR EACH ROW EXECUTE PROCEDURE update_lastmodified_column();

CREATE TABLE category
(
  id serial PRIMARY KEY,
  cname text NOT NULL,
  ename text NOT NULL,
  is_active boolean NOT NULL DEFAULT FALSE,
  parent_id int,
  product_count uint2 NOT NULL DEFAULT 0,
  last_modified timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
  unique (parent_id, cname),
  unique (parent_id, ename)
);
CREATE TRIGGER category_update_last_modified BEFORE UPDATE
ON category FOR EACH ROW EXECUTE PROCEDURE update_lastmodified_column();

CREATE TABLE product_category
(
  id serial PRIMARY KEY,
  upc text NOT NULL,
  category_id int NOT NULL REFERENCES category (id) ON DELETE CASCADE ON UPDATE CASCADE,
  last_modified timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
  unique (upc, category_id)
);
CREATE TRIGGER product_category_update_last_modified BEFORE UPDATE
ON product_category FOR EACH ROW EXECUTE PROCEDURE update_lastmodified_column();

CREATE TABLE vendor_category
(
  id serial PRIMARY KEY,
  vendor_id int NOT NULL REFERENCES vendor (id) ON DELETE CASCADE ON UPDATE CASCADE,
  category_id int NOT NULL REFERENCES category (id) ON DELETE CASCADE ON UPDATE CASCADE,
  vendor_category_id text NOT NULL,
  last_modified timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
  -- unique (vendor_id, category_id),
  unique (vendor_id, vendor_category_id)
);
CREATE TRIGGER vendor_category_update_last_modified BEFORE UPDATE
ON vendor_category FOR EACH ROW EXECUTE PROCEDURE update_lastmodified_column();

CREATE TABLE vendor_product
(
  id serial PRIMARY KEY,
  vendor_id int NOT NULL REFERENCES vendor (id) ON DELETE CASCADE ON UPDATE CASCADE,
  upc text,
  sku text NOT NULL,
  name text NOT NULL,
  brand_name text,
  brand_icon_url text,
  model_number text,
  description text,
  specs jsonb,
  is_product_on_sale boolean NOT NULL,
  regular_price money,
  sale_price money NOT NULL,
  sale_start_date timestamp WITH TIME ZONE,
  sale_end_date timestamp WITH TIME ZONE,
  customer_rating decimal(3, 2) DEFAULT 0,
  customer_rating_count uint2 DEFAULT 0,
  images text[],
  source_url text NOT NULL,
  last_modified timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
  unique (vendor_id, sku),
  unique (vendor_id, upc)
);
CREATE TRIGGER vendor_product_update_last_modified BEFORE UPDATE
ON vendor_product FOR EACH ROW EXECUTE PROCEDURE update_lastmodified_column();
CREATE TRIGGER vendor_brand_name_model_number_upper_trim BEFORE INSERT OR UPDATE
ON vendor_product FOR EACH ROW EXECUTE PROCEDURE upper_trim_on_insert_update();
CREATE INDEX IF NOT EXISTS idx_vendor_product_upc ON vendor_product (upc text_pattern_ops) WHERE upc IS NOT NULL;

CREATE TABLE vendor_product_record
(
  id serial PRIMARY KEY,
  vendor_product_id int NOT NULL REFERENCES vendor_product (id) ON DELETE CASCADE ON UPDATE CASCADE,
  record_date date NOT NULL DEFAULT CURRENT_DATE,
  sales int NOT NULL DEFAULT 0,
  pageview int NOT NULL DEFAULT 0,
  unique (vendor_product_id, record_date)
);

CREATE TABLE vendor_store
(
  id serial PRIMARY KEY,
  vendor_id int NOT NULL REFERENCES vendor (id) ON DELETE CASCADE ON UPDATE CASCADE,
  location_id text NOT NULL,
  name text,
  address text NOT NULL,
  city text,
  state text,
  country text,
  landmark  text,
  postal_code text,
  phone text,
  geom geometry,
  last_modified timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
  unique (vendor_id, location_id)
);
CREATE TRIGGER vendor_store_update_last_modified BEFORE UPDATE
ON vendor_store FOR EACH ROW EXECUTE PROCEDURE update_lastmodified_column();
CREATE INDEX IF NOT EXISTS idx_vendor_store_geom ON vendor_store USING gist (geom);

DROP TYPE IF EXISTS weekday;
CREATE TYPE weekday AS ENUM ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'Holiday');

CREATE TABLE vendor_store_hours
(
  id serial PRIMARY KEY,
  vendor_store_id int NOT NULL REFERENCES vendor_store (id) ON DELETE CASCADE ON UPDATE CASCADE,
  day weekday NOT NULL,
  open text,
  close text,
  last_modified timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
  unique (vendor_store_id, day)
);
CREATE TRIGGER vendor_store_hours_update_last_modified BEFORE UPDATE
ON vendor_store_hours FOR EACH ROW EXECUTE PROCEDURE update_lastmodified_column();

CREATE TABLE vendor_product_store_pickup
(
  id serial PRIMARY KEY,
  vendor_product_id int NOT NULL REFERENCES vendor_product (id) ON DELETE CASCADE ON UPDATE CASCADE,
  vendor_store_id int NOT NULL REFERENCES vendor_store (id) ON DELETE CASCADE ON UPDATE CASCADE,
  has_inventory boolean NOT NULL DEFAULT TRUE,
  quantity uint2 NOT NULL DEFAULT 0,
  last_modified timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
CREATE TRIGGER vendor_product_store_pickup_update_last_modified BEFORE UPDATE
ON vendor_product_store_pickup FOR EACH ROW EXECUTE PROCEDURE update_lastmodified_column();
ALTER TABLE vendor_product_store_pickup ADD CONSTRAINT vendor_product_in_store CHECK (check_vendor_product_in_store(vendor_product_id, vendor_store_id));

CREATE TABLE vendor_product_shipping
(
  id serial PRIMARY KEY,
  vendor_product_id int NOT NULL REFERENCES vendor_product (id) ON DELETE CASCADE ON UPDATE CASCADE,
  purchasable boolean NOT NULL DEFAULT TRUE,
  quantity uint2 NOT NULL DEFAULT 0,
  last_modified timestamp with time zone DEFAULT CURRENT_TIMESTAMP
);
CREATE TRIGGER vendor_product_shipping_update_last_modified BEFORE UPDATE
ON vendor_product_shipping FOR EACH ROW EXECUTE PROCEDURE update_lastmodified_column();

CREATE MATERIALIZED VIEW IF NOT EXISTS product AS
(
  WITH get_average_sales_and_pageview_daily AS
  (
    SELECT DISTINCT ON (vp.upc)
      vp.upc,
      round(SUM(vpr.sales)::numeric / (CURRENT_DATE - min(vpr.record_date) + 1), 2) AS average_sales_daily,
      round(SUM(vpr.pageview)::numeric / (CURRENT_DATE - min(vpr.record_date) + 1), 2) AS average_pageview_daily
    FROM vendor_product_record AS vpr
    JOIN vendor_product AS vp ON vp.id = vpr.vendor_product_id
    WHERE vp.upc IS NOT NULL
    GROUP BY vp.upc
  ),
  get_product AS
  (
    SELECT DISTINCT ON (vp.upc)
      vp.upc,
      first_value(vp.brand_icon_url)          OVER (PARTITION BY SUBSTRING(vp.brand_name FROM '[^[:space:]]+') ORDER BY LENGTH(vp.brand_icon_url) ASC NULLS LAST) AS brand_icon_url,
      first_value(vp.name)                    OVER (PARTITION BY vp.upc ORDER BY LENGTH(vp.name) DESC NULLS LAST)        AS name,
      first_value(vp.brand_name)              OVER (PARTITION BY vp.upc ORDER BY LENGTH(vp.brand_name) ASC NULLS LAST) AS brand_name,
      first_value(vp.model_number)            OVER (PARTITION BY vp.upc ORDER BY LENGTH(vp.model_number) DESC NULLS LAST) AS model_number,
      first_value(vp.description)             OVER (PARTITION BY vp.upc ORDER BY LENGTH(vp.description) DESC NULLS LAST) AS description,
      first_value(vp.specs)                   OVER (PARTITION BY vp.upc ORDER BY JSONB_ARRAY_LENGTH(vp.specs) DESC NULLS LAST) AS specs,
      first_value(v.small_image_url_prefix)   OVER (PARTITION BY vp.upc ORDER BY ARRAY_LENGTH(vp.images, 1) DESC NULLS LAST) AS small_image_url_prefix,
      first_value(v.medium_image_url_prefix)  OVER (PARTITION BY vp.upc ORDER BY ARRAY_LENGTH(vp.images, 1) DESC NULLS LAST) AS medium_image_url_prefix,
      first_value(v.large_image_url_prefix)   OVER (PARTITION BY vp.upc ORDER BY ARRAY_LENGTH(vp.images, 1) DESC NULLS LAST) AS large_image_url_prefix,
      first_value(vp.images)                  OVER (PARTITION BY vp.upc ORDER BY ARRAY_LENGTH(vp.images, 1) DESC NULLS LAST) AS images,
      first_value(vp.is_product_on_sale)      OVER (PARTITION BY vp.upc ORDER BY vp.sale_price ASC, customer_rating DESC) AS is_product_on_sale,
      first_value(vp.regular_price)           OVER (PARTITION BY vp.upc ORDER BY vp.sale_price ASC, customer_rating DESC) AS regular_price,
      first_value(vp.sale_price)              OVER (PARTITION BY vp.upc ORDER BY vp.sale_price ASC, customer_rating DESC) AS sale_price,
      first_value(vp.customer_rating)         OVER (PARTITION BY vp.upc ORDER BY vp.sale_price ASC, customer_rating DESC) AS customer_rating,
      first_value(vp.customer_rating_count)   OVER (PARTITION BY vp.upc ORDER BY vp.sale_price ASC, customer_rating DESC) AS customer_rating_count,
      coalesce(vpr.average_sales_daily, 0) AS average_sales_daily,
      coalesce(vpr.average_pageview_daily, 0) AS average_pageview_daily
    FROM vendor_product AS vp
    JOIN vendor AS v ON v.id = vp.vendor_id
    LEFT JOIN get_average_sales_and_pageview_daily AS vpr ON vpr.upc = vp.upc
    WHERE vp.upc IS NOT NULL
  )
  SELECT
    *,
    setweight(to_tsvector('simple', coalesce(brand_name, '')), 'A') ||
    setweight(to_tsvector('simple', coalesce(model_number, '')), 'B') ||
    setweight(to_tsvector('simple', name), 'C') ||
    setweight(to_tsvector('simple', coalesce(description, '')), 'D') as document
  FROM get_product
);
CREATE UNIQUE INDEX IF NOT EXISTS idx_product_upc ON product (upc text_pattern_ops);
CREATE INDEX IF NOT EXISTS idx_product_brand_name ON product (brand_name text_pattern_ops);
CREATE INDEX IF NOT EXISTS idx_product_average_sales_daily_sale_price ON product (average_sales_daily, average_pageview_daily, sale_price);
CREATE INDEX IF NOT EXISTS idx_product_specs ON product USING gin (specs);
CREATE INDEX IF NOT EXISTS idx_product_document ON product USING gin (document);


CREATE MATERIALIZED VIEW word AS
SELECT word FROM ts_stat('SELECT document FROM product');
CREATE INDEX IF NOT EXISTS idx_word ON word USING gin(word gin_trgm_ops);


CREATE OR REPLACE FUNCTION get_pickup_sales_by_vendor_id(vendorId int)
RETURNS TABLE (vendor_store_id int, vendor_product_id int, record_date DATE, sales int) AS
$BODY$
    SELECT vendor_store_id, vendor_product_id, t.last_modified::DATE,
    SUM(
       CASE WHEN next_last_modified > t.last_modified THEN
              quantity - next_quantity
            ELSE NULL
       END
    )::int AS sales
    FROM (
        SELECT vendor_store_id, vendor_product_id, last_modified::DATE, quantity,
        LEAD(last_modified) OVER w AS next_last_modified,
        LEAD(quantity) OVER w AS next_quantity
        FROM vendor_product_store_pickup
        WINDOW w AS (PARTITION BY vendor_store_id, vendor_product_id, last_modified::DATE ORDER BY last_modified)
    ) AS t
    LEFT JOIN vendor_store AS vs ON vs.id = vendor_store_id
    WHERE vs.vendor_id = vendorId
    GROUP BY vendor_store_id, vendor_product_id, t.last_modified::DATE;
$BODY$ LANGUAGE sql STABLE STRICT;

CREATE OR REPLACE FUNCTION get_shipping_sales_by_vendor_id(vendorId int)
RETURNS TABLE (vendor_product_id int, record_date DATE, sales int) AS
$BODY$
    SELECT vendor_product_id, t.last_modified::DATE,
    SUM(
       CASE WHEN next_last_modified > t.last_modified THEN
              quantity - next_quantity
            ELSE NULL
       END
    )::int AS sales
    FROM (
        SELECT vendor_product_id, last_modified::DATE, quantity,
        LEAD(last_modified) OVER w AS next_last_modified,
        LEAD(quantity) OVER w AS next_quantity
        FROM vendor_product_shipping
        WINDOW w AS (PARTITION BY vendor_product_id, last_modified::DATE ORDER BY last_modified)
    ) AS t
    LEFT JOIN vendor_product AS vp ON vp.id = vendor_product_id
    WHERE vp.vendor_id = vendorId
    GROUP BY vendor_product_id, t.last_modified::DATE;
$BODY$ LANGUAGE sql STABLE STRICT;

CREATE OR REPLACE FUNCTION get_pickup_and_shipping_sales_by_vendor_id(vendorId int)
RETURNS TABLE (vendor_product_id int, record_date DATE, sales int) AS
$BODY$
    SELECT vendor_product_id, record_date, SUM(sales)::int AS sales
    FROM (
        SELECT vendor_product_id, record_date, SUM(sales) AS sales FROM get_pickup_sales_by_vendor_id(vendorId) WHERE sales > 0
        GROUP BY vendor_product_id, record_date
    UNION ALL
        SELECT vendor_product_id, record_date, SUM(sales) AS sales FROM get_shipping_sales_by_vendor_id(vendorId) WHERE sales > 0
        GROUP BY vendor_product_id, record_date
    ) AS t
    GROUP BY vendor_product_id, record_date;
$BODY$ LANGUAGE sql STABLE STRICT;

CREATE OR REPLACE FUNCTION update_product_view() RETURNS void AS $$
BEGIN
  REFRESH MATERIALIZED VIEW CONCURRENTLY product;
END
$$ LANGUAGE PLPGSQL VOLATILE;

CREATE OR REPLACE FUNCTION update_word_view() RETURNS void AS $$
BEGIN
  REFRESH MATERIALIZED VIEW word;
END
$$ LANGUAGE PLPGSQL VOLATILE;

CREATE OR REPLACE FUNCTION clean_vendor_product_record_by_vendor_id(vendorId int) RETURNS void AS $$
BEGIN
  DELETE FROM vendor_product_record AS vpr
   USING vendor_product AS vp
   WHERE vp.id = vpr.vendor_product_id AND vp.vendor_id = vendorId AND vpr.record_date < (CURRENT_DATE - 365);
  DELETE FROM vendor_product_shipping AS vps
   USING vendor_product AS vp
   WHERE vp.id = vps.vendor_product_id AND vp.vendor_id = vendorId AND vps.last_modified::DATE < CURRENT_DATE;
  DELETE FROM vendor_product_store_pickup AS vpsp
   USING vendor_store AS vs
   WHERE vs.id = vpsp.vendor_store_id AND vs.vendor_id = vendorId AND vpsp.last_modified::DATE < CURRENT_DATE;
END
$$ LANGUAGE PLPGSQL VOLATILE STRICT;

CREATE OR REPLACE FUNCTION update_vendor_product_record_by_vendor_id(vendorId int) RETURNS void AS $$
BEGIN
  INSERT INTO vendor_product_record (vendor_product_id, record_date, sales)
   SELECT * FROM get_pickup_and_shipping_sales_by_vendor_id(vendorId)
   ON CONFLICT ON CONSTRAINT vendor_product_record_vendor_product_id_record_date_key DO
   UPDATE SET sales = EXCLUDED.sales;
  PERFORM clean_vendor_product_record_by_vendor_id(vendorId);
  PERFORM update_product_view();
  PERFORM update_word_view();
END
$$ LANGUAGE PLPGSQL VOLATILE STRICT;

CREATE OR REPLACE FUNCTION get_leaf_categories_by_parent_id(parentId int)
RETURNS SETOF category AS
$BODY$
    WITH RECURSIVE leaf_categories AS
    (
        SELECT *
        FROM category
        WHERE parent_id = parentId
    UNION ALL
        SELECT c.*
        FROM leaf_categories b, category c
        WHERE b.id = c.parent_id
    )
    SELECT * FROM leaf_categories
    WHERE id NOT IN (SELECT parent_id FROM category WHERE parent_id IS NOT NULL);
$BODY$ LANGUAGE sql STABLE STRICT;

CREATE OR REPLACE FUNCTION get_itself_and_children_categories_by_parent_id(categoryId int)
RETURNS SETOF category AS
$BODY$
    WITH RECURSIVE leaf_categories AS
    (
        SELECT *
        FROM category
        WHERE id = categoryId
    UNION ALL
        SELECT c.*
        FROM leaf_categories b, category c
        WHERE b.id = c.parent_id
    )
    SELECT * FROM leaf_categories;
$BODY$ LANGUAGE sql STABLE STRICT;

CREATE OR REPLACE FUNCTION get_parent_categories_by_id(categoryId int)
RETURNS SETOF category AS
$BODY$
    WITH RECURSIVE parent_categories AS
    (
        SELECT *
        FROM category
        WHERE id = categoryId
    UNION ALL
        SELECT c.*
        FROM parent_categories p, category c
        WHERE p.parent_id = c.id
    )
    SELECT * FROM parent_categories;
$BODY$ LANGUAGE sql STABLE STRICT;

CREATE OR REPLACE FUNCTION get_all_categories_by_category_id(categoryId int)
RETURNS SETOF category AS
$BODY$
    SELECT * FROM get_parent_categories_by_id(categoryId)
    UNION
    SELECT * FROM get_itself_and_children_categories_by_parent_id(categoryId);
$BODY$ LANGUAGE sql STABLE STRICT;

CREATE OR REPLACE FUNCTION get_category_path_by_upc(product_upc text)
RETURNS SETOF category AS
$BODY$
    SELECT DISTINCT pids.* FROM product_category pc
    JOIN LATERAL get_all_categories_by_category_id(pc.category_id) AS pids
    ON pc.upc = product_upc;
$BODY$ LANGUAGE sql STABLE STRICT;


INSERT INTO vendor (name, small_image_url_prefix, medium_image_url_prefix, large_image_url_prefix, icon_url)
 VALUES ('Best Buy', '//multimedia.bbycastatic.ca/multimedia/products/55x55/{0}', '//multimedia.bbycastatic.ca/multimedia/products/150x150/{0}', '//multimedia.bbycastatic.ca/multimedia/products/500x500/{0}', '//cdn2-b.examiner.com/sites/default/files/styles/image_content_width/hash/62/ea/2000px-best_buy_logo-svg.png?itok=P-kKrTJd');
INSERT INTO vendor (name, small_image_url_prefix, medium_image_url_prefix, large_image_url_prefix, icon_url)
 VALUES ('Memory Express', '//media.memoryexpress.com/Images/Products/Small/{0}', '//media.memoryexpress.com/Images/Products/Default/{0}', '//media.memoryexpress.com/Images/Products/Large/{0}', '//lh5.googleusercontent.com/-YeeacshimKE/VlevWqjoG-I/AAAAAAAAAAU/wteTAlZPubE/s1600/');
INSERT INTO vendor (name, small_image_url_prefix, medium_image_url_prefix, large_image_url_prefix, icon_url)
 VALUES ('Staples', '//www.staples-3p.com/s7/is/image/Staples/{0}?$thb$', '//www.staples-3p.com/s7/is/image/Staples/{0}?$std$', '//www.staples-3p.com/s7/is/image/Staples/{0}?$splssku$', '//www.staples.ca/sbdpas/img/ico/home_logo.png');

INSERT INTO category (cname, ename, is_active, parent_id, product_count) VALUES ('全部分类', 'All Categories', TRUE, DEFAULT, 0);
INSERT INTO category (cname, ename, is_active, parent_id, product_count) VALUES ('电脑 & 平板 & 电子阅读器', 'Computers & Tablets & eReaders', TRUE, 1, 0);
INSERT INTO category (cname, ename, is_active, parent_id, product_count) VALUES ('鼠标 & 键盘', 'Mice & Keyboards', TRUE, 1, 0);
INSERT INTO category (cname, ename, is_active, parent_id, product_count) VALUES ('游戏键盘', 'Gaming Keyboards', TRUE, 3, 0);
INSERT INTO category (cname, ename, is_active, parent_id, product_count) VALUES ('键盘鼠标套装', 'Keyboard & Mouse Combos', TRUE, 3, 0);
INSERT INTO category (cname, ename, is_active, parent_id, product_count) VALUES ('辅助键盘', 'Keypads', TRUE, 3, 0);
INSERT INTO category (cname, ename, is_active, parent_id, product_count) VALUES ('游戏鼠标', 'Gaming Mice', TRUE, 3, 0);
INSERT INTO category (cname, ename, is_active, parent_id, product_count) VALUES ('有线鼠标', 'Wired Mice', TRUE, 3, 0);
INSERT INTO category (cname, ename, is_active, parent_id, product_count) VALUES ('无线鼠标', 'Wireless Mice', TRUE, 3, 0);
INSERT INTO category (cname, ename, is_active, parent_id, product_count) VALUES ('笔记本电脑', 'Laptops & Ultrabooks', TRUE, 2, 0);

INSERT INTO vendor_category (vendor_id, category_id, vendor_category_id) VALUES (1, 4, '24055');
INSERT INTO vendor_category (vendor_id, category_id, vendor_category_id) VALUES (2, 4, 'Keyboards_Gaming');
INSERT INTO vendor_category (vendor_id, category_id, vendor_category_id) VALUES (1, 5, '20391');
INSERT INTO vendor_category (vendor_id, category_id, vendor_category_id) VALUES (2, 5, 'Keyboards_DesktopSets');
INSERT INTO vendor_category (vendor_id, category_id, vendor_category_id) VALUES (2, 6, 'Keyboards_Keypads');
INSERT INTO vendor_category (vendor_id, category_id, vendor_category_id) VALUES (1, 7, '29584');
INSERT INTO vendor_category (vendor_id, category_id, vendor_category_id) VALUES (2, 7, 'Mouse_Gaming');
INSERT INTO vendor_category (vendor_id, category_id, vendor_category_id) VALUES (1, 8, '20390');
INSERT INTO vendor_category (vendor_id, category_id, vendor_category_id) VALUES (2, 8, 'Mouse_Wired');
INSERT INTO vendor_category (vendor_id, category_id, vendor_category_id) VALUES (1, 9, '20395');
INSERT INTO vendor_category (vendor_id, category_id, vendor_category_id) VALUES (2, 9, 'Mouse_Wireless');

INSERT INTO vendor_category (vendor_id, category_id, vendor_category_id) VALUES (1, 10, '20352');
INSERT INTO vendor_category (vendor_id, category_id, vendor_category_id) VALUES (2, 10, 'LaptopsNotebooks');
INSERT INTO vendor_category (vendor_id, category_id, vendor_category_id) VALUES (3, 10, 'CL200591');
