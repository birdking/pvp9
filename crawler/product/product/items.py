# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field


class ProductItem(Item):
    id = Field()
    upc = Field()
    name = Field()
    brand_name = Field()
    brand_icon_url = Field()
    model_number = Field()
    description = Field()
    specs = Field()

    category_id = Field()
    vendor_id = Field()
    vendor_product_id = Field()
    sku = Field()
    is_product_on_sale = Field()
    regular_price = Field()
    sale_price = Field()
    sale_start_date = Field()
    sale_end_date = Field()
    customer_rating = Field()
    customer_rating_count = Field()
    source_url = Field()

    locations = Field()
    shipping = Field()

    images = Field()
