# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import simplejson as json
import psycopg2
import psycopg2.extras
psycopg2.extras.register_json(oid=3802, array_oid=3807, globally=True)
from product.constants import *

class ProductPipeline(object):

    def normalizeUpcNumber(self, upc):
        if upc and len(upc.strip()) > 0:
            ucc14 = upc.strip().zfill(14)
            if ucc14 != '00000000000000':
                return ucc14
        return None

    def process_item(self, item, spider):
        vendor_product_id = item['vendor_product_id']
        if vendor_product_id:
            return item
        upc = self.normalizeUpcNumber(item['upc'])
        name = item['name']
        brand_name = item['brand_name']
        brand_icon_url = item['brand_icon_url']
        model_number = item['model_number']
        description = item['description']
        specs = json.dumps(item['specs'])
        category_id = item['category_id']
        vendor_id = item['vendor_id']
        sku = item['sku']
        is_product_on_sale = item['is_product_on_sale']
        regular_price = item['regular_price']
        sale_price = item['sale_price']
        sale_start_date = item['sale_start_date']
        sale_end_date = item['sale_end_date']
        customer_rating = item['customer_rating']
        customer_rating_count = item['customer_rating_count']
        source_url = item['source_url']
        images = item['images']

        with psycopg2.connect(DB_CONFIG) as conn:
            with conn.cursor() as cur:
                # insert or update product_mapping
                cur.execute(
                    """
                    INSERT INTO vendor_product (vendor_id, upc, sku, name, brand_name, brand_icon_url, model_number,
                     description, specs, is_product_on_sale, regular_price, sale_price,
                     sale_start_date, sale_end_date, customer_rating, customer_rating_count,
                     images, source_url)
                     VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                     ON CONFLICT ON CONSTRAINT vendor_product_vendor_id_sku_key DO
                     UPDATE SET upc = EXCLUDED.upc, name = EXCLUDED.name, brand_name = EXCLUDED.brand_name,
                     brand_icon_url = EXCLUDED.brand_icon_url, model_number = EXCLUDED.model_number, description = EXCLUDED.description, specs = EXCLUDED.specs,
                     is_product_on_sale = EXCLUDED.is_product_on_sale,
                     regular_price = EXCLUDED.regular_price, sale_price = EXCLUDED.sale_price,
                     sale_start_date = EXCLUDED.sale_start_date, sale_end_date = EXCLUDED.sale_end_date,
                     customer_rating = EXCLUDED.customer_rating, customer_rating_count = EXCLUDED.customer_rating_count,
                     images = EXCLUDED.images, source_url = EXCLUDED.source_url RETURNING id;
                    """,
                    [vendor_id, upc, sku, name, brand_name, brand_icon_url, model_number,
                     description, specs, is_product_on_sale, regular_price, sale_price,
                     sale_start_date, sale_end_date, customer_rating, customer_rating_count,
                     images, source_url])
                item['vendor_product_id'] = cur.fetchone()[0]

                if upc:
                    cur.execute(
                        """
                        INSERT INTO product_category (upc, category_id) VALUES(%s, %s)
                         ON CONFLICT ON CONSTRAINT product_category_upc_category_id_key DO NOTHING;
                        """,
                        [upc, category_id])

                return item


class AvailabilityPipeline(object):
    def process_item(self, item, spider):
        vendor_id = item['vendor_id']
        vendor_product_id = item['vendor_product_id']
        locations = item['locations']
        shipping = item['shipping']

        if locations or shipping:
            with psycopg2.connect(DB_CONFIG) as conn:
                with conn.cursor() as cur:
                    if shipping:
                        purchasable = shipping['purchasable']
                        shipping_quantity = shipping['quantity']
                        cur.execute(
                            """
                            INSERT INTO vendor_product_shipping (vendor_product_id, purchasable, quantity)
                             VALUES (%s, %s, %s);
                            """,
                            [vendor_product_id,
                             purchasable,
                             shipping_quantity])

                    if locations:
                        for location in locations:
                            location_id = location['location_id']
                            has_inventory = location['has_inventory']
                            quantity = location['quantity']

                            cur.execute(
                                """
                                INSERT INTO vendor_product_store_pickup (vendor_product_id, vendor_store_id, has_inventory, quantity)
                                 VALUES (%s, (SELECT id FROM vendor_store WHERE vendor_id = %s AND location_id = %s), %s, %s)
                                """,
                                [vendor_product_id,
                                 vendor_id,
                                 location_id,
                                 has_inventory,
                                 quantity])
