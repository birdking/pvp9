import simplejson as json
from datetime import datetime
import scrapy
import re
from product.constants import *
from product.items import ProductItem


class BestbuyProductSpider(scrapy.Spider):
    name = "bestbuy_product"

    def format_sale_date(self, date):
        if date:
          try:
            return datetime.strptime(date, '%d/%m/%Y %I:%M:%S %p').strftime('%Y-%m-%d %H:%M:%S')
          except:
            pass
        return None

    def closed(self, reason):
        update_product_sales_record(BESTBUY['ID'])

    def start_requests(self):
        locations = '%7C'.join(get_locations(BESTBUY['ID']))
        avalability_prefix = BESTBUY['AVAILABILITY_API_PREFIX'] + '&locations=' + locations

        for category in get_categories(BESTBUY['ID']):
            yield scrapy.Request(BESTBUY['PRODUCT_SEARCH_API_PREFIX'] + '?categoryid=' + category[1] + '&pageSize=100&page=1',
                                 meta={'category_id': category[0], 'vendor_category_id': category[1], 'avalability_prefix': avalability_prefix},
                                 callback=self.parse_category)

    def parse_category(self, response):
        json_response = json.loads(response.body)

        currentPage = int(json_response['currentPage'])
        totalPages = int(json_response['totalPages'])
        if currentPage < totalPages:
            yield scrapy.Request(BESTBUY['PRODUCT_SEARCH_API_PREFIX'] + '?categoryid=' + response.meta['vendor_category_id'] + '&pageSize=100&page=' + str(currentPage + 1),
                                 meta=response.meta,
                                 callback=self.parse_category)

        for product in json_response['products']:
            sku = product['sku']
            yield scrapy.Request(BESTBUY['PRODUCT_DETAIL_API_PREFIX'] + sku,
                               meta=response.meta,
                               callback=self.parse_product)

    def parse_product(self, response):
        json_response = json.loads(response.body)

        item = ProductItem()
        item['vendor_product_id'] = None
        item['upc'] = json_response['upcNumber']
        item['name'] = json_response['name']
        item['brand_name'] = json_response['brandName']
        item['brand_icon_url'] = json_response['brandThumbnailImage']
        item['model_number'] = json_response['modelNumber']
        item['description'] = json_response['shortDescription'] if json_response['shortDescription'] else json_response['longDescription']
        item['specs'] = json_response['specs']

        item['category_id'] = response.meta['category_id']
        item['vendor_id'] = BESTBUY['ID']
        item['sku'] = json_response['sku'].lower()
        item['is_product_on_sale'] = json_response['isProductOnSale']
        item['regular_price'] = json_response['regularPrice']
        item['sale_price'] = json_response['salePrice']
        item['sale_start_date'] = self.format_sale_date(json_response['SaleStartDate'])
        item['sale_end_date'] = self.format_sale_date(json_response['SaleEndDate'])
        item['customer_rating'] = json_response['customerRating']
        item['customer_rating_count'] = json_response['customerRatingCount']
        item['source_url'] = json_response['productUrl']
        item['locations'] = None
        item['shipping'] = None
        item['images'] = None
        # item['images'] = [item['sku'][:3] + '/' + item['sku'][:5] + '/' + item['sku'] + '.jpg'] if item['sku'] else None
        # thumbnailImageReg = re.compile('^https:\/\/multimedia\.bbycastatic\.ca\/multimedia\/products\/55x55\/([0-9]{3})\/([0-9]{5})\/%s\.(jpe?g|png|gif|bmp)$'%item['sku'], re.IGNORECASE)
        if json_response['thumbnailImage']:
            image_match = re.match(r'^(https|http):\/\/multimedia\.bbycastatic\.ca\/multimedia\/products\/(55x55|100x100|150x150|250x250|300x300|400x400|500x500)\/(.*)', json_response['thumbnailImage'], re.IGNORECASE)
            if image_match:
                item['images'] = [image_match.group(3)]

        response.meta['productItem'] = item
        url = response.meta['avalability_prefix'] + '&skus=' + item['sku'].upper()
        yield scrapy.Request(url, meta=response.meta, callback=self.parse_availability)

    def parse_availability(self, response):
        productItem = response.meta['productItem']
        json_response = json.loads(response.body)

        for availability in json_response['availabilities']:
            pickup = availability['pickup']
            shipping = availability['shipping']
            shipping_quantity = shipping['quantityRemaining'] if shipping['quantityRemaining'] else 0

            productItem['locations'] = [{'location_id': location['locationKey'], 'has_inventory': location['hasInventory'], 'quantity': 0} for location in pickup['locations']]
            productItem['shipping'] = {'purchasable': shipping['purchasable'], 'quantity': shipping_quantity}
            yield productItem

    # start_urls = [
    #     category_url_prefix + 'Departments',
    # ]

    # def parse(self, response):
    #     parent_id = None
    #     if 'parent_id' in response.meta:
    #         parent_id = response.meta['parent_id']
    #
    #     jsonresponse = json.loads(response.body)

        # print parent_id

        # items = []
        # for category in jsonresponse['subCategories']:
            # subitem = CategoryItem()
            # print category['id']
            # print category['name']
            # subitem['id'] = category['id']
            # subitem['name'] = category['name']
            # subitem['parent_id'] = category['id']
            # if category['hasSubcategories']:
            #     yield scrapy.Request(constants.bestbuy.CATEGORY_API_PREFIX + category['id'], meta={'parent_id': jsonresponse['id']})

            # items.append(item)


class MemoryExpressProductSpider(scrapy.Spider):
    name = "memoryexpress_product"

    def closed(self, reason):
        update_product_sales_record(MEMORYEXPRESS['ID'])

    def start_requests(self):
        locations = get_locations(MEMORYEXPRESS['ID'])
        for category in get_categories(MEMORYEXPRESS['ID']):
            yield scrapy.Request(MEMORYEXPRESS['CATEGORY_PREFIX'] + category[1],
                                 meta={'category_id': category[0], 'vendor_category_id': category[1], 'locations': locations},
                                 callback=self.parse_category)

    def parse_category(self, response):
        product_ids = response.xpath('//div[contains(@class, "PIV_Body")]/div[contains(@class, "ProductId")]/text()').extract()
        for product_id in product_ids:
            yield scrapy.Request(MEMORYEXPRESS['PRODUCT_PREFIX'] + product_id,
                                 meta=response.meta,
                                 callback=self.parse_product)

        next_page = response.xpath('//div[contains(@class, "AJAX_List_Pager")]/ul/li[contains(@class, "AJAX_List_Pager_Next")]/a/@href').extract_first()
        if next_page:
            yield scrapy.Request(MEMORY_EXPRESS_PREFIX + next_page,
                                 meta=response.meta,
                                 callback=self.parse_category)

    def format_sale_date(self, date):
        if date:
          try:
            return datetime.strptime(date, 'Sale Ends: %b %d, %Y %H:%M').strftime('%Y-%m-%d %H:%M:%S')
          except:
            pass
        return None


    def parse_product(self, response):
        item = ProductItem()

        item['vendor_product_id'] = None
        item['name'] = response.xpath('//div[@id="PD_Header"]/div[@class="PDH_Left"]/div[@class="PDH_HeaderBlock"]/h1/text()').extract_first().strip()
        item['brand_name'] = None
        item['brand_icon_url'] = None
        brand = response.xpath('//div[@id="PD_Header"]/div[@class="PDH_Left"]/div[@class="PDH_HeaderBlock"]/div[1]')
        brand_a = brand.xpath('a').extract_first()
        brand_h1_name = brand.xpath('h1/text()').extract_first()
        if brand_a:
            item['brand_name'] =  brand.xpath('a/@href').extract_first().strip()[7:]
            item['brand_icon_url'] = brand.xpath('a/img/@src').extract_first().strip()
        elif brand_h1_name:
            item['brand_name'] =  brand_h1_name.strip()
        item['sku'] = response.xpath('//div[@id="ProductAdd"]/div[@class="Details"]/ul/li[1]/text()').extract_first().strip()
        item['upc'] = response.xpath('//div[@id="ProductAdd"]/div[@class="Details"]/ul/li[2]/text()').extract_first().strip()
        item['model_number'] = response.xpath('//div[@id="ProductAdd"]/div[@class="Details"]/ul/li[3]/text()').extract_first().strip()
        item['description'] = response.xpath('//div[@id="PDTab_Information"]/div[@class="PBody_Description"]/h3[text() = "Product Info"]/following-sibling::div[1]').extract_first()
        item['description'] = re.sub('\s*\r\n*\s*', '', item['description']) if item['description'] else None
        item['specs'] = []
        specs = response.xpath('//div[@id="PDTab_Information"]/div[@class="PBody_Description"]/h3[text() = "Specifications"]/following-sibling::div[1]/table/tr')
        for spec in specs:
            spec_group = spec.xpath('*[1]/text()').extract_first()
            spec_objs = spec.xpath('td[position()=last()]/text()[preceding-sibling::br or following-sibling::br]')
            spec_obj_text = spec.xpath('td[position()=last()]/text()').extract_first()
            if spec_group and spec_obj_text:
                if spec_objs.extract_first():
                    for spec_obj in spec_objs.extract():
                        spec_obj_text = spec_obj.strip()
                        if spec_obj_text:
                            spec_match = re.match(r'(.*)\s?\:\s?(.*)', spec_obj_text)
                            if spec_match:
                                item['specs'].append({'group': spec_group, 'name': spec_match.group(1), 'value': spec_match.group(2)})
                            else:
                                item['specs'].append({'group': spec_group, 'name': '', 'value': spec_obj_text})
                else:
                    item['specs'].append({'group': '', 'name': spec_group, 'value': spec_obj_text})

        item['category_id'] = response.meta['category_id']
        item['vendor_id'] = MEMORYEXPRESS['ID']

        item['regular_price'] = response.xpath('//div[@id="ProductPricing"]/div[@class="RegularPrice"]/span[@class="Price"]/text()').extract_first()
        item['sale_price'] = response.xpath('//div[@id="ProductPricing"]/div[@class="Totals"]/div[@class="GrandTotal"]/div[1]/text()').extract_first().strip()
        item['is_product_on_sale'] = False
        item['sale_start_date'] = None
        item['sale_end_date'] = response.xpath('//div[@id="ProductPricing"]/div[@class="EndDate"]/text()').extract_first()
        if item['sale_end_date']:
            item['sale_end_date'] = self.format_sale_date(item['sale_end_date'][:-4])
            item['is_product_on_sale'] = True
        customer = response.xpath('//div[@id="PD_Header"]/div[@class="PDH_Left"]/div[@class="PDH_Right"]/div[@class="PDH_BottomAligned"]/div[@class="PR_Large"]')
        item['customer_rating'] = customer.xpath('div[1]/a/span[@class="PR_Text"]/text()').extract_first().strip()
        if item['customer_rating']:
            item['customer_rating'] = re.match(r'([0-9\.]{1,3})/([0-9\.]{1,3})', item['customer_rating']).group(1)
        item['customer_rating_count'] = customer.xpath('div[@class="PR_SubText"]/strong/text()').extract_first()
        item['source_url'] = response.url

        item['shipping'] = None
        shipping_quantity = response.xpath('//div[@class="OnlineStore"]/span[contains(@class, "Availability")]/text()').extract_first()
        if shipping_quantity:
            quantity_match = re.match(r'^([0-9]+)(\+?)', shipping_quantity, re.IGNORECASE)
            real_quantity = int(quantity_match.group(1)) if quantity_match else 0
            purchasable = True if real_quantity > 0 else False if shipping_quantity  == 'Out of Stock' else True
            item['shipping'] = {'purchasable': purchasable, 'quantity': real_quantity}

        item['images'] = []
        images = response.xpath('//div[@id="PDPI"]/div[@class="PDPI_Images"]/ul/li/img/@src').extract()
        for image in images:
            image_match = re.match('^(https|http):\/\/media.memoryexpress.com\/Images\/Products\/%s\/([0-9]+)\?'%item['sku'], image)
            if image_match:
                item['images'].append(item['sku'] + '-' + image_match.group(2) + '.jpg')

        item['locations'] = []
        stores = response.xpath( '//div[@id="ProductInventory_Drop"]/div[@class="Region"]/div[@class="Stores"]/ul/li//span[@class="Store"]')
        for store in stores:
            location = store.xpath('text()').extract_first()
            quantity = store.xpath('following-sibling::span[contains(@class, "Availability")]/text()').extract_first()
            if location and quantity and location in response.meta['locations']:
                quantity = quantity.strip()
                quantity_match = re.match(r'^([0-9]+)(\+?)', quantity)
                real_quantity = int(quantity_match.group(1)) if quantity_match else 0
                has_inventory = False if quantity == 'Out of Stock' else True if real_quantity > 0 else False
                item['locations'].append({'location_id': location, 'has_inventory': has_inventory, 'quantity': real_quantity})
        return item


class StaplesProductSpider(scrapy.Spider):
    name = "staples_product"

    def closed(self, reason):
        update_product_sales_record(STAPLES['ID'])

    def start_requests(self):
        locations = get_locations(STAPLES['ID'])
        for category in get_categories(STAPLES['ID']):
            yield scrapy.Request(STAPLES['CATEGORY_PREFIX'].format(category[1]),
                                 cookies={'zipcode': 'V5K1C1'},
                                 meta={'category_id': category[0], 'vendor_category_id': category[1], 'locations': locations},
                                 callback=self.parse_category)

    def parse_category(self, response):
        product_ids = response.xpath('//ul[@id="productDetail"]/li//a[@class="url"]/@href').extract()
        for product_id in product_ids:
            yield scrapy.Request(STAPLES['PRODUCT_PREFIX'].format(product_id),
                                 cookies={'zipcode': 'V5K1C1'},
                                 meta=response.meta,
                                 callback=self.parse_product)

        next_page = response.xpath('//div[@class="perpage"]/ul/li[contains(@class, "active")]/following-sibling::a[@class="hide"]/@href').extract_first()
        if next_page:
            yield scrapy.Request(next_page,
                                 cookies={'zipcode': 'V5K1C1'},
                                 meta=response.meta,
                                 callback=self.parse_category)

    def format_sale_date(self, date):
        if date:
          try:
            return datetime.strptime('Offer Expires on 03/29/2016', 'Offer Expires on %m/%d/%Y').strftime('%Y-%m-%d %H:%M:%S')
          except:
            pass
        return None

    def parse_product(self, response):
        item = ProductItem()

        item['vendor_product_id'] = None
        productCatalog = response.xpath('//xml[@id="skufilterbrowse"]/productcatalog/product')
        item['name'] = productCatalog.xpath('@name').extract_first().strip()
        item['sku'] = productCatalog.xpath('@snum').extract_first().strip()
        item['source_url'] = productCatalog.xpath('//div[contains(@class, "yotpo-main-widget")]/@data-url').extract_first().strip()
        item['upc'] = response.xpath('//div[contains(@class, "yotpo-main-widget")]/@data-product-spec-upc').extract_first().strip()
        item['description'] = response.xpath('//div[contains(@class, "yotpo-main-widget")]/@data-description').extract_first()
        item['brand_name'] = productCatalog.xpath('@brandname').extract_first().strip()
        item['brand_icon_url'] = None
        item['model_number'] = productCatalog.xpath('@mnum').extract_first()
        item['specs'] = []
        specs = response.xpath('//table[@id="tableSpecifications"]/tr')
        for spec in specs:
            spec_name = spec.xpath('td[1]/text()').extract_first()
            spec_value = spec.xpath('td[2]/text()').extract_first()
            item['specs'].append({'group': '', 'name': spec_name, 'value': spec_value})

        item['category_id'] = response.meta['category_id']
        item['vendor_id'] = STAPLES['ID']
        item['regular_price'] = productCatalog.xpath('price/@was').extract_first()
        item['sale_price'] = productCatalog.xpath('price/@is').extract_first().strip()
        item['is_product_on_sale'] = bool(productCatalog.xpath('price/@special').extract_first())
        item['sale_start_date'] = None
        item['sale_end_date'] = productCatalog.xpath('descs/desc[@typeid="35"]/text()').extract_first()
        if item['sale_end_date']:
            item['sale_end_date'] = self.format_sale_date(item['sale_end_date'])
            item['is_product_on_sale'] = True
        item['customer_rating'] = productCatalog.xpath('review/@rating').extract_first()
        item['customer_rating_count'] = productCatalog.xpath('review/@count').extract_first()
        shipping_quantity = productCatalog.xpath('delivery/@instore').extract_first()
        item['shipping'] = {'purchasable': bool(shipping_quantity), 'quantity': int(shipping_quantity) if shipping_quantity else 0}
        images = productCatalog.xpath('imgs/pic[contains(text(), "$std$")]/text()').extract()
        item['images'] = []
        for image in images:
            # http://www.staples-3p.com/s7/is/image/Staples/s0991121_sc7?$std$
            image_match = re.match('^(https|http):\/\/www.staples-3p.com\/s7\/is\/image\/Staples\/(\w+)\?\$std\$', image)
            if image_match:
                item['images'].append(image_match.group(2))

        response.meta['productItem'] = item
        url = STAPLES['LOCATION_API_PREFIX'].format(item['sku'])
        yield scrapy.Request(url,
                             cookies={'zipcode': 'V5K1C1'},
                             meta=response.meta,
                             callback=self.parse_availability)

    def parse_availability(self, response):
        productItem = response.meta['productItem']
        locations = response.meta['locations']
        availabilities = response.xpath('//table[@id="resultsTbl"]/tbody/tr')
        productItem['locations'] = []

        for availability in availabilities:
            store_number = availability.xpath('td[1]/input[contains(@id, "storeNumber")]/@value').extract_first()
            if store_number in locations:
                quantity = availability.xpath('td[3]/p/text()').extract_first()
                quantity_match = re.match(r'^([0-9]+)(\+?)available', ''.join(quantity.split()), re.IGNORECASE)
                real_quantity = int(quantity_match.group(1)) if quantity_match else 0
                has_inventory = True if real_quantity > 0 else False
                productItem['locations'].append({'location_id': store_number, 'has_inventory': has_inventory, 'quantity': real_quantity})
        return productItem
