# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field


class StoreItem(Item):
    id = Field()
    vendor_id = Field()
    location_id = Field()
    name = Field()
    is_active = Field()
    address = Field()
    city = Field()
    state = Field()
    country = Field()
    landmark = Field()
    postal_code = Field()
    phone = Field()
    lat = Field()
    lng = Field()
    hours = Field()
