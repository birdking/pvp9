import psycopg2
import psycopg2.extras
psycopg2.extras.register_json(oid=3802, array_oid=3807, globally=True)


DB_CONFIG = 'dbname=pvp9 host=db port=5432 user=postgres password=sm01314'

WEEKDAY = ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'Holiday')

BESTBUY_API_PREFIX = 'http://www.bestbuy.ca/api/v2/json'
BESTBUY = {
    'ID': 1,
    'CATEGORY_API_PREFIX': BESTBUY_API_PREFIX + '/category/',
    'PRODUCT_SEARCH_API_PREFIX': BESTBUY_API_PREFIX + '/search',
    'PRODUCT_DETAIL_API_PREFIX': BESTBUY_API_PREFIX + '/product/',
    'LOCATION_API_PREFIX': BESTBUY_API_PREFIX + '/locations?postalcode=v6c3e1',
    'AVAILABILITY_API_PREFIX': 'http://api.bestbuy.ca/availability/products?accept-language=en&accept=application%2Fvnd.bestbuy.standardproduct.v1%2Bjson',
}

MEMORY_EXPRESS_PREFIX = 'http://www.memoryexpress.com'
MEMORYEXPRESS = {
    'ID': 2,
    'STORE_PREFIX': MEMORY_EXPRESS_PREFIX + '/Stores',
    'CATEGORY_PREFIX': MEMORY_EXPRESS_PREFIX + '/Category/',
    'PRODUCT_PREFIX': MEMORY_EXPRESS_PREFIX + '/Products/',
}

STAPLES_PREFIX = 'http://www.staples.ca/en'
STAPLES = {
    'ID': 3,
    'STORE_PREFIX': 'http://storelocator.staples.ca/stores.xml?latitude=49.2891158&longitude=-123.1168909&radius=7000&locale=en_CA&offset=0&limit=7000',
    'STORE_HOURS_PREFIX': 'http://storelocator.staples.ca/stores/en_CA/{state}/{city}/{store_number}',
    'CATEGORY_PREFIX': 'http://www.staples.ca/en/cat_{0}_2-CA_1_20001',
    'PRODUCT_PREFIX': 'http://www.staples.ca/en/product_{0}_2-CA_1_20001',
    'LOCATION_API_PREFIX' : 'http://www.staples.ca/office/supplies/StaplesStoreInventory?langId=1&partNumber={0}&zipCode=v6c3e1&distance=999',
}

def format_hours(time, clock):
    if time and clock:
        return time + ' ' + clock
    return None

def get_categories(vendor_id):
    with psycopg2.connect(DB_CONFIG) as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT c.id, vc.vendor_category_id FROM get_leaf_categories_by_parent_id(1) AS c
                 LEFT JOIN vendor_category AS vc ON vc.category_id = c.id WHERE vc.vendor_id = %s;
                """, [vendor_id])
            return cur.fetchall()

def get_locations(vendor_id):
    with psycopg2.connect(DB_CONFIG) as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT location_id FROM vendor_store WHERE vendor_id = %s;
                """, [vendor_id])
            return [location[0] for location in cur.fetchall()]

def update_product_sales_record(vendor_id):
    with psycopg2.connect(DB_CONFIG) as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT update_vendor_product_record_by_vendor_id(%s);
                """, [vendor_id])
