# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import psycopg2
import psycopg2.extras
from store.constants import *
psycopg2.extras.register_json(oid=3802, array_oid=3807, globally=True)


class PostgresqlPipeline(object):

    def process_item(self, item, spider):
        vendor_id = item['vendor_id']
        location_id = item['location_id']
        name = item['name']
        address = item['address']
        city = item['city']
        state = item['state']
        country = item['country']
        landmark = item['landmark']
        postal_code = item['postal_code']
        phone = item['phone']
        lat = item['lat']
        lng = item['lng']
        hours = item['hours']

        with psycopg2.connect(DB_CONFIG) as conn:
            with conn.cursor() as cur:
                # insert or update product
                cur.execute(
                    """
                    INSERT INTO vendor_store (vendor_id, location_id, name, address, city, state, country, landmark, postal_code, phone, geom)
                     VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, ST_SetSRID(ST_MakePoint(%s, %s),4326))
                     ON CONFLICT ON CONSTRAINT vendor_store_vendor_id_location_id_key DO
                     UPDATE SET name = EXCLUDED.name, address = EXCLUDED.address, city = EXCLUDED.city, state = EXCLUDED.state,
                     country = EXCLUDED.country, landmark = EXCLUDED.landmark, postal_code = EXCLUDED.postal_code,
                     phone = EXCLUDED.phone, geom = EXCLUDED.geom RETURNING id;
                    """,
                    [vendor_id,
                     location_id,
                     name,
                     address,
                     city,
                     state,
                     country,
                     landmark,
                     postal_code,
                     phone,
                     lng,
                     lat])
                vendor_store_id = cur.fetchone()[0]

                for hour in hours:
                    hours_day = hour['day']
                    hours_open = hour['open']
                    hours_close = hour['close']

                    # insert or update hours
                    cur.execute(
                        """
                        INSERT INTO vendor_store_hours (vendor_store_id, day, open, close)
                         VALUES(%s, %s, %s, %s)
                         ON CONFLICT ON CONSTRAINT vendor_store_hours_vendor_store_id_day_key DO
                         UPDATE SET day = EXCLUDED.day, open = EXCLUDED.open, close = EXCLUDED.close;
                        """,
                        [vendor_store_id, hours_day, hours_open, hours_close])
                return item
