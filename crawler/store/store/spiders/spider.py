import simplejson as json
import scrapy
from store.constants import *
import re
from store.items import StoreItem
import urllib2
import urllib


class BestbuySpider(scrapy.Spider):
    name = 'bestbuy_store'

    start_urls = [
        BESTBUY['LOCATION_API_PREFIX'],
    ]

    def parse(self, response):
        json_response = json.loads(response.body)
        locations = json_response['locations']

        for location in locations:
            item = StoreItem()
            item['vendor_id'] = BESTBUY['ID']
            item['location_id'] = location['locationId']
            item['name'] = location['name']
            item['address'] = location['address1']
            item['city'] = location['city']
            item['state'] = location['region']
            item['country'] = location['country']
            item['landmark'] = location['landmark']
            item['postal_code'] = location['postalCode']
            item['phone'] = location['phone1'] if location['phone1'] else location['phone2'] if location['phone2'] else location['phone3']
            item['lat'] = location['lat']
            item['lng'] = location['lng']

            hours = []
            for hour in location['hours']:
                time_match = re.match(r'(Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday)([0-9\:]{1,5})(AM|PM)-([0-9:]{1,5})(AM|PM)', ''.join(hour.split()), re.IGNORECASE)
                if time_match and time_match.group(1) in WEEKDAY:
                    hours_day = time_match.group(1)
                    hours_open = format_hours(time_match.group(2), time_match.group(3))
                    hours_close = format_hours(time_match.group(4), time_match.group(5))
                    hours.append({'day': hours_day, 'open': hours_open, 'close': hours_close})
            item['hours'] = hours
            yield item


class MemoryExpressSpider(scrapy.Spider):
    name = 'memoryexpress_store'
    allowed_domains = ["memoryexpress.com"]

    start_urls = [
        MEMORYEXPRESS['STORE_PREFIX'],
    ]

    def parse(self, response):
        location_id_mapping = {
            'Calgary North East': 'Calgary NE',
            'Calgary North West': 'Calgary NW',
            'Calgary South East': 'Calgary SE',
            'Edmonton North': 'Edmonton North',
            'Edmonton South': 'Edmonton South',
            'Vancouver Richmond': 'Richmond BC',
            'Winnipeg West': 'Winnipeg West'
        }
        selector = response.selector
        provinces = selector.xpath('//div[@id="StoreList"]/div[@class="Province"]')
        for province in provinces:
            # province_name = province.xpath('h2/text()').extract()
            locations = province.xpath('div[@class="LocationSummary"]')
            for location in locations:
                item = StoreItem()
                item['vendor_id'] = MEMORYEXPRESS['ID']
                item['name'] = location.xpath('div[@class="Header"]/a/h3/text()').extract_first().strip()
                item['location_id'] = location_id_mapping[item['name']]
                location_info = location.xpath('div[@class="Address"]/span/text()').extract()
                item['address'] = location_info[0].strip()
                location_match = re.match(r'^(.*),\s(.*)\s(.*)\s(.*)$', location_info[1].strip()) # Calgary, AB T1Y 5X7
                item['city'] = location_match.group(1)
                item['state'] = location_match.group(2)
                item['country'] = 'CAN'
                item['landmark'] = None
                item['postal_code'] = location_match.group(3) + ' ' + location_match.group(4)
                item['phone'] = re.match(r'^Phone:\s(.*)$', location_info[2].strip()).group(1)
                geocoding_url = 'http://www.mapquestapi.com/geocoding/v1/address?key=ImrzQ7KyLO2eERp8vt8HUOitZnp8WiFs&location=' + urllib.quote_plus(item['postal_code'])
                geocoding = json.loads(urllib2.urlopen(geocoding_url).read())
                lat_lng = geocoding['results'][0]['locations'][0]['latLng']
                item['lat'] = lat_lng['lat']
                item['lng'] = lat_lng['lng']

                location_hours = location.xpath('div[@class="StoreHours"]/div')
                hours = []
                for location_hour in location_hours:
                    hours_day = location_hour.xpath('span[@class="Label"]/text()').extract_first().strip()
                    hours_time = location_hour.xpath('text()|*[not(self::span)]/text()').extract_first().strip()
                    time_match = re.match(r'([0-9\:]{1,5})(AM|PM)-([0-9\:]{1,5})(AM|PM)', ''.join(hours_time.split()), re.IGNORECASE)
                    hours_open = format_hours(time_match.group(1), time_match.group(2).upper())
                    hours_close = format_hours(time_match.group(3), time_match.group(4).upper())
                    if hours_day == 'Mon-Fri':
                        hours.append({'day': 'Monday', 'open': hours_open, 'close': hours_close})
                        hours.append({'day': 'Tuesday', 'open': hours_open, 'close': hours_close})
                        hours.append({'day': 'Wednesday', 'open': hours_open, 'close': hours_close})
                        hours.append({'day': 'Thursday', 'open': hours_open, 'close': hours_close})
                        hours.append({'day': 'Friday', 'open': hours_open, 'close': hours_close})
                    elif hours_day == 'Holidays':
                        hours.append({'day': 'Holiday', 'open': hours_open, 'close': hours_close})
                    elif hours_day in WEEKDAY:
                        hours.append({'day': hours_day, 'open': hours_open, 'close': hours_close})
                item['hours'] = hours
                yield item


class StaplesSpider(scrapy.Spider):
    name = 'staples_store'

    start_urls = [
        STAPLES['STORE_PREFIX'],
    ]

    def parse(self, response):
        selector = response.selector
        stores = selector.xpath('//store')
        for store in stores:
            item = StoreItem()
            item['vendor_id'] = STAPLES['ID']
            item['name'] = None
            item['location_id'] = store.xpath('store_number/text()').extract_first()
            item['lat'] = float(store.xpath('latitude/text()').extract_first())
            item['lng'] = float(store.xpath('longitude/text()').extract_first())
            item['address'] = store.xpath('store_address/address_line1/text()').extract_first()
            item['city'] = store.xpath('store_address/city/text()').extract_first()
            item['state'] = store.xpath('store_address/state/text()').extract_first()
            item['country'] = store.xpath('store_address/country/text()').extract_first()
            item['postal_code'] = store.xpath('store_address/zip/text()').extract_first()
            item['phone'] = store.xpath('store_address/phone_number/text()').extract_first()
            item['landmark'] = None
            url = STAPLES['STORE_HOURS_PREFIX'].format(state=item['state'], city=item['city'], store_number=item['location_id'])
            yield scrapy.Request(url, meta={'productItem': item}, callback=self.parse_hours)

    def parse_hours(self, response):
        productItem = response.meta['productItem']
        selector = response.selector
        location_hours = selector.xpath('//div[@class="storeTime"]/h3[text() = "Store Hours"]/following-sibling::div[1]/span/text()').extract()
        hours = []
        for location_hour in location_hours:
            time_match = re.match(r'(Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday)\:([0-9\:]{1,5})(AM|PM)-([0-9:]{1,5})(AM|PM)', ''.join(location_hour.split()), re.IGNORECASE)
            if time_match and time_match.group(1) in WEEKDAY:
                hours_day = time_match.group(1)
                hours_open = format_hours(time_match.group(2), time_match.group(3).upper())
                hours_close = format_hours(time_match.group(4), time_match.group(5).upper())
                hours.append({'day': hours_day, 'open': hours_open, 'close': hours_close})
        productItem['hours'] = hours
        return productItem
