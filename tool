#!/usr/bin/env bash
#


usage() {
	cat <<-EOF

  Usage: tool [command]

  Options:

    -h, --help             output help information

  Commands:

    crawler                log into crawler container
    postgresql             log into postgresql
    nginx                  log into vip nodejs nginx
    db                     log into db mysql container
    dbip                   show the ip of db mysql container
    up                     shorthand for docker-compose up
    rebuild                rebuild develop enviroment
    stop                   shorthand for docker-compose stop
    ps                     shorthand for docker-compose ps
    rm                     shorthand for docker-compose rm -f
EOF
}


inspect() {
  docker exec -it $1 sh
}

dbip() {
  docker exec -it postgresql ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/'
}

while test $# -ne 0; do
	arg=$1; shift
  case $arg in
    -h|--help) usage; exit ;;
    crawler) inspect rank-crawler; exit ;;
    postgresql) inspect rank-postgresql; exit ;;
    nginx) inspect rank-nginx; exit ;;
    dbip) dbip; exit ;;
    up) docker-compose up; exit ;;
    rebuild) sudo rm -rf ./postgresql/data; docker-compose rm -f; docker-compose up; exit ;;
    stop) docker-compose stop; exit ;;
    ps) docker-compose ps; exit ;;
    rm) docker-compose rm -f; exit ;;
  esac
done
